﻿using CommonEngine.ScriptableObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour {

    [SerializeField] GameObject menu;
    [SerializeField] Button openButton;
    [SerializeField] ColorPreset menuOnButtonColor;
    [SerializeField] ColorPreset menuOffButtonColor;
    [SerializeField] AudioObject openCloseSound;

    void Awake()
    {
        openButton.onClick.AddListener(() => OpenCloseMenu());
    }

    public void HideMenuButton()
    {
        openButton.interactable = false;
        openButton.GetComponent<Outline>().enabled = false;
        GetComponent<ImageFade>().Fade();
    }

    public void OpenCloseMenu()
    {
        menu.gameObject.SetActive(!menu.gameObject.activeInHierarchy);
        openButton.GetComponent<Image>().color = menu.gameObject.activeInHierarchy ? menuOnButtonColor : menuOffButtonColor;
        openButton.GetComponent<Outline>().enabled = menu.gameObject.activeInHierarchy;
        Time.timeScale = menu.gameObject.activeInHierarchy ? 0 : 1;
        SoundManager.Instance.PlayFX(openCloseSound);
    }

}
