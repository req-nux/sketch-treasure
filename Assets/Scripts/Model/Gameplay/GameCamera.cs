﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour {

    public void FocusOnMapElement(MapElement elem)
    {
        transform.position = new Vector3(elem.transform.position.x, elem.transform.position.y, transform.position.z);
    }
}
