﻿
public interface IMoveable {

    bool CanMove(Direction dir);
    void Move(Direction dir);
    void Move(Field field);

    float FieldTravelSpeed { get; set; }

}
