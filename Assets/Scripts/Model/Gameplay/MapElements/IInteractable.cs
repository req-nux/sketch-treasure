﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable {

    void Interact();

    /// <summary>
    /// Checks if player is in range to interact with this element
    /// </summary>
    /// <returns>True if player is directly up, down, right or left to this element</returns>
    bool InInteractionRange();
    bool CanInteract();
    bool IsDuringInteraction();

    void OnInteractionEnd();

}
