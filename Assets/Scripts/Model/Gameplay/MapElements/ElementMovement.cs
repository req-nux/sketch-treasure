﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Moves element to destination field
/// </summary>
public class ElementMovement : MonoBehaviour {

    protected float movementStepLenght;
    protected MapElement elem;
    protected ElementAnimator anim;

    protected bool isMoving;

    protected virtual void Awake()
    {
        elem = GetComponent<MapElement>();
        anim = GetComponent<ElementAnimator>();
        movementStepLenght = LevelMap.FieldSize / ((IMoveable)elem).FieldTravelSpeed;
    }

    void OnEnable()
    {
        CheckpointLevelReset.onLevelReset += OnCheckpointLevelReset;
    }

    public void Move(Direction dir)
    {
        StartCoroutine(MoveCoroutine(dir, ((IMoveable)elem).FieldTravelSpeed));
    }

    public virtual void Move(Field field)
    {

    }

    protected IEnumerator MoveCoroutine(Direction dir, float travelTime)
    {
        isMoving = true;
        Vector3 direction = Formatter.directionToVec2(dir);
        Vector3 destination = transform.position + direction;
        LevelMap.Instance.MoveElement(elem, elem.MapPosition + Formatter.vector3To2(direction));
        
        while((transform.position - destination).magnitude > movementStepLenght * Time.deltaTime)
        {
            transform.position += movementStepLenght * direction * Time.deltaTime;
            yield return null;
        }
        transform.position = destination;
        isMoving = false;
    }

    void OnCheckpointLevelReset()
    {
        StopAllCoroutines();
        isMoving = false;
    }

    void OnDisable()
    {
        CheckpointLevelReset.onLevelReset -= OnCheckpointLevelReset;
    }

    public bool IsMoving
    {
        get { return isMoving; }
    }
}
