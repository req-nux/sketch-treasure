﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHighlighter {

    void SetHighlight(bool highlighted);
}
