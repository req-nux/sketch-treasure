﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Web : MonoBehaviour, IConfigurable {

    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Transform spriteTransform;

    public ElementConfig GetConfig()
    {
        return new WebConfig(transform.position,spriteRenderer.flipX, spriteRenderer.flipY, spriteTransform.localPosition);
    }

    public void SetSpriteByConfig(WebConfig webConfig)
    {
        spriteRenderer.flipX = webConfig.flipX;
        spriteRenderer.flipY = webConfig.flipY;
        spriteTransform.localPosition = webConfig.spritePos;
    }
}

[System.Serializable]
public class WebConfig : ElementConfig {

    public Vector3 pos;
    public bool flipX;
    public bool flipY;
    public Vector3 spritePos;

    public WebConfig(Vector3 pos, bool flipX, bool flipY, Vector3 spritePos)
    {
        this.pos = pos;
        this.flipX = flipX;
        this.flipY = flipY;
        this.spritePos = spritePos;
    }
}
