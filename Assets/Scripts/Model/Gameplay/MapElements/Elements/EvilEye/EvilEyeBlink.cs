﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvilEyeBlink : MonoBehaviour {

    [SerializeField] float minBlinkInterval;
    [SerializeField] float maxBlinkInterval;
    [SerializeField] float openedYScale;
    [SerializeField] float closedYScale;
    [SerializeField] float blinkSpeed = 0.2f;
    float nextBlinkTime;

    void Awake()
    {
        nextBlinkTime = Time.time + Random.Range(minBlinkInterval, maxBlinkInterval);
    }
    void Update()
    {
        if(Time.time > nextBlinkTime)
        {
            nextBlinkTime += Random.Range(minBlinkInterval, maxBlinkInterval);
            StartCoroutine(BlinkCoroutine());
        }
    }
    IEnumerator BlinkCoroutine()
    {
        float yScale;
        while(transform.localScale.y > closedYScale)
        {
            yScale = Mathf.Max(transform.localScale.y - blinkSpeed * Time.deltaTime, closedYScale);
            transform.localScale = new Vector3(transform.localScale.x, yScale, transform.localScale.z);
            yield return null;
        }
        while(transform.localScale.y < openedYScale)
        {
            yScale = Mathf.Min(transform.localScale.y + blinkSpeed * Time.deltaTime, openedYScale);
            transform.localScale = new Vector3(transform.localScale.x, yScale, transform.localScale.z);
            yield return null;
        }
    }
}
