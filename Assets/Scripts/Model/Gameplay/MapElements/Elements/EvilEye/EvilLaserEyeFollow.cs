﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvilLaserEyeFollow : MonoBehaviour {

    [SerializeField] Transform eye;
    [SerializeField] float maxEyeOffset;
    Transform playerTransform;

    void Awake()
    {
        playerTransform = LevelMap.Player.transform;
    }
    void Update()
    {
        eye.localPosition = (playerTransform.position - eye.position).normalized * maxEyeOffset;
    }
}
