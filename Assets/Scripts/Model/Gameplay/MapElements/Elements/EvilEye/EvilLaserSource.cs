﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvilLaserSource : MapElement {

    [SerializeField] float playerHitDelay = 1f;
    [SerializeField] GameObject laser;
    [SerializeField] CommonEngine.ScriptableObjects.AudioObject attackSound;
    bool playerHit = false;

    void OnEnable()
    {
        LevelMap.OnElementMoved += TryHitPlayer;
    }
    void OnDisable()
    {
        LevelMap.OnElementMoved -= TryHitPlayer;
    }

    void TryHitPlayer()
    {
        if(playerHit)
            return;

        Direction directionToPlayer = LevelMap.Player.DirectionFromElement(this);
        if(directionToPlayer == Direction.Undefined)
            return;

        if(CanHitPlayer(directionToPlayer))
        {
            playerHit = true;
            StartCoroutine(DelayHitPlayer(directionToPlayer));
        }
    }

    IEnumerator DelayHitPlayer(Direction directionToPlayer)
    {
        yield return new WaitForSeconds(playerHitDelay);
        EnableLaser(directionToPlayer);
        SoundManager.Instance.PlayFX(attackSound);
        LevelMap.Player.Death();
    }

    void EnableLaser(Direction dir)
    {
        switch(dir)
        {
            case Direction.Right: laser.transform.Rotate(Vector3.back * 0);break;
            case Direction.Down: laser.transform.Rotate(Vector3.back * 90);break;
            case Direction.Left: laser.transform.Rotate(Vector3.back * 180);break;
            case Direction.Up: laser.transform.Rotate(Vector3.back * 270);break;
        }
        float distanceToPlayer = (MapPosition - LevelMap.Player.MapPosition).magnitude + 0.5f;
        laser.transform.localScale = new Vector3(laser.transform.localScale.x, distanceToPlayer, laser.transform.localScale.z);
        laser.SetActive(true);
    }

    bool CanHitPlayer(Direction dir)
    {
        int i = 1;
        Vector2 dirVector = Formatter.directionToVec2(dir);
        Field checkedField = LevelMap.Instance.GetElement<Field>(MapPosition + dirVector * i);
        while(checkedField != null && checkedField.ElemOnField == null)
        {
            i++;
            checkedField = LevelMap.Instance.GetElement<Field>(MapPosition + dirVector * i);
        }
        return checkedField != null && checkedField.ElemOnField == LevelMap.Player;
    }
}
