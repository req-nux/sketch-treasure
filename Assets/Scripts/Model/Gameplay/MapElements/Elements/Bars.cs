﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CloseDirection { Up, Right }

public class Bars : MapElement , IConfigurable {

    [SerializeField] CloseDirection closeDir;
    [SerializeField] bool opened = false;
    [SerializeField] CommonEngine.ScriptableObjects.AudioObject barsRattleSoundOpen;
    [SerializeField] CommonEngine.ScriptableObjects.AudioObject barsRattleSoundClose;
    ElementAnimator animator;
    Collider2D barsCollider;

    void Awake()
    {
        animator = GetComponent<ElementAnimator>();
        barsCollider = GetComponent<Collider2D>();
    }
    public ElementConfig GetConfig()
    {
        return new BarsConfig(transform.position, opened, closeDir);
    }

    public void initState(bool opened)
    {
        this.opened = opened;
        if(barsCollider != null)
            barsCollider.enabled = !opened;
        LevelMap.Instance.GetElement<Field>(MapPosition).ElemOnField = opened ? null : this;
        if(animator != null && opened)
            animator.PlayStateImmediate("BarsOpened");
    }

    public void changeState(bool muteSound = false)
    {
        animator.HandleBarsStateChange(!opened, closeDir);
        opened = !opened;
        barsCollider.enabled = !barsCollider.enabled;
        LevelMap.Instance.GetElement<Field>(MapPosition).ElemOnField = opened ? null : this;
        if(!muteSound)
            SoundManager.Instance.PlayFX(opened ? barsRattleSoundOpen : barsRattleSoundClose);
    }
    public void changeStateImmediate()
    {
        opened = !opened;
        barsCollider.enabled = !barsCollider.enabled;
        animator.PlayStateImmediate(opened ? "BarsOpened" : "BarsClosed");
        LevelMap.Instance.GetElement<Field>(MapPosition).ElemOnField = opened ? null : this;
    }

    IEnumerator waitForStateChange()
    {
        yield return new WaitForSeconds(1);
        opened = !opened;
        LevelMap.Instance.GetElement<Field>(MapPosition).ElemOnField = opened ? null : this;
    }

    public bool Opened
    {
        get { return opened; }
        set { opened = value; }
    }

    public CloseDirection CloseDirection
    {
        set { closeDir = value; }
    }
}
