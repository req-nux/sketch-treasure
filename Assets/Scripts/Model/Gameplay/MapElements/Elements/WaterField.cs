﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterField : Field
{
    [SerializeField] GameObject insertedRock;
    [SerializeField] CommonEngine.ScriptableObjects.AudioObject splashSound;
    bool isFilled = false;

    public override bool IsOccupied()
    {
        return !isFilled || isFilled && elemOnField != null;
    }

    public bool CanPutRock()
    {
        return !isFilled || !IsOccupied();
    }

    IEnumerator OnRockInsertCoroutine(Rock rock)
    {
        yield return new WaitUntil(() => !rock.IsDuringInteraction());
        if(Time.timeSinceLevelLoad > 1f)
            SoundManager.Instance.PlayFX(splashSound);
        Destroy(rock.gameObject);
        isFilled = true;
        insertedRock.SetActive(true);
    }
        
    public override MapElement ElemOnField
    {
        get { return elemOnField; }
        set
        {
            if(!isFilled && !(value is Rock))
                Debug.LogErrorFormat("{0} can't be on WaterField, when its not filled!", value.ToString());

            if(!isFilled)
            {
                StartCoroutine(OnRockInsertCoroutine((Rock)value));
                
            }
            else
            {
                elemOnField = value;
            }
        }
    }

}
