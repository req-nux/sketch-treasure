﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MapElement
{
    [SerializeField] CommonEngine.ScriptableObjects.AudioObject doorCreekSound;
    
    void OnEnable()
    {
        LevelMap.OnElementMoved += OnElemMoved;
    }
    void OnDisable()
    {
        LevelMap.OnElementMoved -= OnElemMoved;
    }

    public override void Interact()
    {
        if(CanInteract())
        {
            SoundManager.Instance.SetMusicVolume(0);
            SoundManager.Instance.PlayFX(doorCreekSound, false, 0.5f);
            ElementAnimator anim = GetComponent<ElementAnimator>();
            anim.HandleDoorOpen();
            LevelMap.Player.Animator.SetState(AnimState.idle);
            float doorOpenTime = anim.GetClipLenghtByName("DoorOpen");
            StartCoroutine(DelayOnLevelComplete(doorOpenTime*2));
        }
        else
        {
            LevelMap.Player.OnInteractionEnd();
        }
    }

    public override void OnInteractionEnd()
    {
        LevelMap.Player.OnInteractionEnd();
    }

    private void OnElemMoved()
    {
        if((MapPosition - LevelMap.Player.MapPosition).magnitude <= 1)
        {
            SoundManager.Instance.SetMusicVolume(SoundManager.Instance.DefaultMusicVolume * 0.25f);
        }
        else if((MapPosition - LevelMap.Player.MapPosition).magnitude <= 2)
        {
            SoundManager.Instance.SetMusicVolume(SoundManager.Instance.DefaultMusicVolume * 0.6f);
        }
        else if((MapPosition - LevelMap.Player.MapPosition).magnitude <= 3)
        {
            SoundManager.Instance.SetMusicVolume(SoundManager.Instance.DefaultMusicVolume);
        }
    }

    IEnumerator DelayOnLevelComplete(float delay)
    {
        yield return new WaitForSeconds(delay);
        GameplayManager.Instance.OnLevelComplete();

    }
}
