﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleLever : MapElement, IConfigurable, IHighlighter {

    [SerializeField] List<Bars> barsState0;
    [SerializeField] List<Bars> barsState1;
    [SerializeField] List<Bars> barsState2;
    [SerializeField] List<Bars> barsState3;
    [SerializeField] [Range(2,4)] int statesCount;
    int state;

    CircleLeverAnimator animator;
    ElementHighlighting elementHighlighting;

    void Awake()
    {
        animator = GetComponent<CircleLeverAnimator>();
        elementHighlighting = GameObject.FindObjectOfType<ElementHighlighting>();
    }

    public ElementConfig GetConfig()
    {
        List<Vector2> bars1Pos = new List<Vector2>();
        List<Vector2> bars2Pos = new List<Vector2>();
        List<Vector2> bars3Pos = new List<Vector2>();
        List<Vector2> bars4Pos = new List<Vector2>();
        foreach(Bars b in barsState0) bars1Pos.Add(b.transform.position);
        foreach(Bars b in barsState1) bars2Pos.Add(b.transform.position);
        foreach(Bars b in barsState2) bars3Pos.Add(b.transform.position);
        foreach(Bars b in barsState3) bars4Pos.Add(b.transform.position);
        return new CircleLeverConfig(transform.position, statesCount, bars1Pos, bars2Pos, bars3Pos, bars4Pos);
    }

    public override void Interact()
    {
        if(CanInteract())
        {
            bool canUseLever = true;
            List<Bars> bars = getBarsBasedOnState(state);
            foreach(Bars b in bars)
                if(b.Opened && LevelMap.Instance.GetElement<Field>(b.MapPosition).IsOccupied())
                    canUseLever = false;
            int nextState = state + 1 < statesCount ? state + 1 : 0;
            List<Bars> nextStateBars = getBarsBasedOnState(nextState);
            foreach(Bars b in nextStateBars)
                if(b.Opened && LevelMap.Instance.GetElement<Field>(b.MapPosition).IsOccupied())
                    canUseLever = false;
            Direction dir = DirectionFromElement(LevelMap.Player);
            if(canUseLever)
            {
                foreach(Bars b in bars)
                    b.changeState();
                foreach(Bars b in nextStateBars)
                    b.changeState();
                animator.handleCircleLeverStateChange(statesCount);
                LevelMap.Player.Animator.HandleRockPushDirection(dir);
                state = state + 1 < statesCount ? state + 1 : 0;
            }
            else
                LevelMap.Player.Animator.HandleRockPushDirection(dir);

            StartCoroutine(waitForInteractionEnd());
        }
        else
            LevelMap.Player.OnInteractionEnd();
    }

    public void setStateImmediate(int newState)
    {
        state = newState;
        animator.setStateImmediate(state, statesCount);
    }

    IEnumerator waitForInteractionEnd()
    {
        yield return new WaitForSeconds(1);
        OnInteractionEnd();
    }

    public void SetHighlight(bool highlighted)
    {
        elementHighlighting.setHighlights(highlighted, getBarsBasedOnState(state));
    }

    List<Bars> getBarsBasedOnState(int s)
    {
        switch(s)
        {
            case 0: return barsState0;
            case 1: return barsState1;
            case 2: return barsState2;
            case 3: default: return barsState3;
        }
    }

    public int State
    {
        get { return state; }
        set { state = value; }
    }
    public int StatesCount
    {
        set { statesCount = value; }
    }
    public List<Bars> Bars0
    {
        get { return barsState0; }
        set { barsState0 = value; }
    }
    public List<Bars> Bars1
    {
        get { return barsState1; }
        set { barsState1 = value; }
    }
    public List<Bars> Bars2
    {
        get { return barsState2; }
        set { barsState2 = value; }
    }
    public List<Bars> Bars3
    {
        get { return barsState3; }
        set { barsState3 = value; }
    }
}

