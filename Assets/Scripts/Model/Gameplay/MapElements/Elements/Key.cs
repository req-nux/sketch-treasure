﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MapElement {

    [SerializeField] PopupInfoPanel keyFoundPanelPrefab;
    RectTransform gameMenuCanvas;
    bool pickedUp;

    void Awake()
    {
        pickedUp = false;
        gameMenuCanvas = GameObject.FindGameObjectWithTag("GameMenuCanvas").GetComponent<RectTransform>();
    }
    public override void Interact()
    {
        if(CanInteract() && !pickedUp)
        {
            PopupInfoPanel keyFoundPanel= Instantiate(keyFoundPanelPrefab, gameMenuCanvas,false) as PopupInfoPanel;
            keyFoundPanel.transform.SetAsFirstSibling();
            keyFoundPanel.Show();
            pickedUp = true;

            SaveKeyFound();
            GetComponent<Collider2D>().enabled = false;
            GetComponentInChildren<SpriteRenderer>().enabled = false;
        }
        LevelMap.Player.OnInteractionEnd();
    }

    void SaveKeyFound()
    {
        int loadedId = GameManager.Instance.LoadedLevel.ID;
        PlayerPrefs.SetInt(string.Format(PersistentDataManager.keyDataString, loadedId), 1);
        Debug.Log("KeyFound saved. keyFieldId: " + loadedId);
    }

}
