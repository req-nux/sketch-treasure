﻿using System.Collections;
using UnityEngine;

public class TreasureHolder : MapElement {

    [SerializeField] Transform diamond;
    [SerializeField] GameObject gameFinishedPanel;
    [SerializeField] float gameFinishedPanelDelay;
    ScrollAnimator scrollAnimator;
    GameMenu menu;

    void Awake()
    {
        scrollAnimator = GameObject.FindObjectOfType<ScrollAnimator>();
        menu = GameObject.FindObjectOfType<GameMenu>();
    }

    public override void Interact()
    {
        if(CanInteract())
        {
            menu.HideMenuButton();
            LevelMap.Player.GetComponent<PlayerController>().enabled = false;
            LevelMap.Player.Animator.SetState(AnimState.treasurePickUp);

            float treasurePickupAnimLenght = LevelMap.Player.Animator.GetClipLenghtByName("TreasurePickup");
            float treasurePickupDelay = treasurePickupAnimLenght * 0.58f;

            StartCoroutine(SetTreasureAsPlayersChild(treasurePickupDelay));
            StartCoroutine(ShowGameFinishedPanel(treasurePickupAnimLenght + gameFinishedPanelDelay));
        }
        else
        {
            LevelMap.Player.OnInteractionEnd();
        }
    }

    IEnumerator SetTreasureAsPlayersChild(float delay)
    {
        Transform hand = LevelMap.Player.transform.Find("sprite/leftHand");
        yield return new WaitForSeconds(delay);
        Time.timeScale = 0.5f;
        diamond.SetParent(hand);
        diamond.transform.localPosition = new Vector3(0, 0, 0);
    }

    IEnumerator ShowGameFinishedPanel(float delay)
    {
        yield return new WaitForSeconds(delay);
        Time.timeScale = 1f;
        scrollAnimator.CloseScroll(() => 
            {
                GameObject finishedPanel = Instantiate(gameFinishedPanel, GameObject.FindGameObjectWithTag("GameMenuCanvas").transform, false);
                finishedPanel.transform.SetAsFirstSibling();
                scrollAnimator.OpenScroll(null);
            });
    }
}
