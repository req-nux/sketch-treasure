﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonField : Field, IConfigurable, IHighlighter
{
    [SerializeField] List<Bars> bars;
    ElementAnimator animator;

    ElementHighlighting elementHighlighting;
    MapElement previousElemOnField;
    bool isDuringStateChange;

    void Awake()
    {
        animator = GetComponent<ElementAnimator>();
        elementHighlighting = GameObject.FindObjectOfType<ElementHighlighting>();
    }

    public ElementConfig GetConfig()
    {
        List<Vector2> barsPos = new List<Vector2>();
        foreach(Bars b in bars)
            barsPos.Add(b.transform.position);
        return new MechanismConfig(transform.position, barsPos);
    }

    public void SetHighlight(bool highlighted)
    {
        elementHighlighting.setHighlights(highlighted, bars);
    }

    IEnumerator ChangeFieldState()
    {
        isDuringStateChange = true;
        bool isElemOnField = elemOnField != null;

        Func<bool> barsCanChangeState = 
            () => Array.TrueForAll(bars.ToArray(), x => !x.Opened || x.Opened && LevelMap.Instance.CanMove(x.MapPosition));
        Func<bool> continueChangeState = 
            () => isElemOnField == (elemOnField != null);

        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => barsCanChangeState() && continueChangeState());

        if(!(previousElemOnField is Rock))
        {
            foreach(Bars b in bars)
                b.changeState();
        }
        animator.HandleButtonFieldStateChange(elemOnField != null);
        isDuringStateChange = false;
    }

    void ChangeFieldStateImmediate()
    {
        
        if(animator == null)
        {
            animator = GetComponent<ElementAnimator>();
            animator.Init();
        }
        animator.PlayStateImmediate(elemOnField != null ? "ButtonFieldPress" : "ButtonFieldRelease");
        foreach(Bars b in bars)
            b.changeStateImmediate();
    }

    public override MapElement ElemOnField
    {
        get { return elemOnField; }
        set
        {
            if(elemOnField != null)
                previousElemOnField = elemOnField;
            elemOnField = value;

            if(!CheckpointLevelReset.isDuringLevelReset && !LevelBuilder.isDuringLevelBuilding)
            {
                if(!isDuringStateChange)
                    StartCoroutine(ChangeFieldState());
            }
            else
            {
                ChangeFieldStateImmediate();
            }
        }
    }

    public List<Bars> Bars
    {
        get { return bars; }
    }
}
