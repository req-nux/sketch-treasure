﻿using System.Collections;
using UnityEngine;

public class Rock : MapElement, IMoveable {

    [SerializeField] float fieldTravelSpeed = 0.7f;
    [SerializeField] CommonEngine.ScriptableObjects.AudioObject rockMoveSound;
    ElementMovement movement;

    void Awake()
    {
        movement = GetComponent<ElementMovement>();
    }

    public bool CanMove(Direction dir)
    {
        Vector2 destMapPos = MapPosition + Formatter.directionToVec2(dir);
        return LevelMap.Instance.CanMove(destMapPos) || 
            LevelMap.Instance.GetElement<WaterField>(destMapPos) != null && LevelMap.Instance.GetElement<WaterField>(destMapPos).CanPutRock();
    }

    public void Move(Field field)
    {
    }

    public void Move(Direction dir)
    {
        movement.Move(dir);
    }

    public override void Interact()
    {
        if(CanInteract() && CanMove(DirectionFromElement(LevelMap.Player)))
        {
            isDuringInteraction = true;
            Direction dir = DirectionFromElement(LevelMap.Player);
            Move(dir);
            SoundManager.Instance.PlayFX(rockMoveSound, true);
            LevelMap.Player.Animator.HandleRockPushDirection(dir);
            LevelMap.Player.Move(dir);
            StartCoroutine(WaitForInteractionEnd());
        }
        else
            LevelMap.Player.OnInteractionEnd();
        
    }
    IEnumerator WaitForInteractionEnd()
    {
        yield return new WaitUntil(() => !LevelMap.Player.Movement.IsMoving);
        OnInteractionEnd();
    }

    public override void OnInteractionEnd()
    {
        isDuringInteraction = false;
        LevelMap.Player.OnInteractionEnd();
    }

    public float FieldTravelSpeed
    {
        get { return fieldTravelSpeed; }
        set { fieldTravelSpeed = value; }
    }

}
