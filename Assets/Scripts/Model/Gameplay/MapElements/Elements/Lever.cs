﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MapElement, IConfigurable, IHighlighter
{

    [SerializeField] List<Bars> bars;
    ElementAnimator animator;
    bool active = false; //false - right

    ElementHighlighting elementHighlighting;

    void Awake()
    {
        animator = GetComponent<ElementAnimator>();
        elementHighlighting = GameObject.FindObjectOfType<ElementHighlighting>();
    }

    public ElementConfig GetConfig()
    {
        List<Vector2> barsPos = new List<Vector2>();
        foreach(Bars b in bars)
            barsPos.Add(b.transform.position);
        return new MechanismConfig(transform.position, barsPos);
    }

    public override void Interact()
    {
        if(CanInteract())
        {
            isDuringInteraction = true;
            bool canUseLever = true;
            foreach(Bars b in bars)
            {
                if(b.Opened && LevelMap.Instance.GetElement<Field>(b.MapPosition).IsOccupied())
                    canUseLever = false;
            }


            Direction dir = DirectionFromElement(LevelMap.Player);
            LevelMap.Player.Animator.HandleLeverUse(dir, this, canUseLever);
            StartCoroutine(waitForInteractionEnd(dir));
        }
        else
        {
            LevelMap.Player.OnInteractionEnd();
        }

    }
    IEnumerator waitForInteractionEnd(Direction dir)
    {
        float delay = 0;
        if(dir == Direction.Down || dir == Direction.Up)
        {
            delay = LevelMap.Player.Animator.GetClipLenghtByName("LeverUpRight_Left");
            if(dir == Direction.Down)
            {
                ChangeMaskEnabled();
                Invoke("ChangeMaskEnabled", delay);
            }
        }
        else
        {
            delay = LevelMap.Player.Animator.GetClipLenghtByName("LeverPushFromLeft_Right");
        }
        yield return new WaitForSeconds(delay);
        OnInteractionEnd();
    }

    public void ChangeBarsState()
    {
        foreach(Bars b in bars)
            b.changeState();
    }

    public override void OnInteractionEnd()
    {
        isDuringInteraction = false;
        active = !active;
        LevelMap.Player.OnInteractionEnd();
    }

    public void SetHighlight(bool highlighted)
    {
        elementHighlighting.setHighlights(highlighted, bars);
    }

    public void changeStateImmediate()
    {
        active = !active;
        animator.PlayStateImmediate(active ? "LeverLeft" : "LeverRight");
    }

    void ChangeMaskEnabled()
    {
        LevelMap.Player.Animator.Mask.enabled = !LevelMap.Player.Animator.Mask.enabled;

        SpriteMask mask = GetComponentInChildren<SpriteMask>();
        SpriteMaskUpdater maskUpdater = GetComponentInChildren<SpriteMaskUpdater>();
        mask.enabled = !mask.enabled;
        maskUpdater.enabled = !maskUpdater.enabled;
    }

    public List<Bars> Bars
    {
        get { return bars; }
    }
    public bool Active
    {
        get { return active; }
    }

}
