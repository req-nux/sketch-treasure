﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSound : MonoBehaviour {

    [SerializeField] CommonEngine.ScriptableObjects.AudioObject[] footsteps;
    PlayerMovement playerMovement;
    float clipFinishedTime;
    int currentClipIndex;
    bool clipStopped;

	void Awake () {
        playerMovement = GetComponent<PlayerMovement>();
    }

    void Update() {

        if(playerMovement.IsMoving)
        {
            if(Time.time > clipFinishedTime)
            {
                clipStopped = false;
                PlayRandomFootstepAudio();
            }
        }
        else if(!clipStopped)
        {
            clipStopped = true;
            SoundManager.Instance.StopClip(footsteps[currentClipIndex].Clip);
        }
	}

    void PlayRandomFootstepAudio()
    {
        currentClipIndex = Random.Range(0, footsteps.Length);
        SoundManager.Instance.PlayFX(footsteps[currentClipIndex]);
        clipFinishedTime = Time.time + footsteps[currentClipIndex].ClipLength*0.8f;
    }
}
