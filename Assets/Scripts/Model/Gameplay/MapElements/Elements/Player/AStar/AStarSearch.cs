﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStarSearch {

    Dictionary<Node, Node[]> nodes;

    PriorityQueue potentialNodes;
    PriorityQueue examinedNodes;
    Node start;
    Node end;
    Node best;

    public AStarSearch()
    {
        Field[,] fieldMap = LevelMap.Instance.Map;
        Node[,] nodeMap = new Node[fieldMap.GetLength(0), fieldMap.GetLength(1)];
        nodes = new Dictionary<Node, Node[]>();
        for(int i = 0; i < fieldMap.GetLength(0); i++)
        {
            for(int j = 0; j < fieldMap.GetLength(1); j++)
            {
                if(fieldMap[i, j] != null)
                {
                    nodeMap[i, j] = new Node(fieldMap[i, j]);
                }
            }
        }
        for(int i = 0; i < nodeMap.GetLength(0); i++)
        {
            for(int j = 0; j < nodeMap.GetLength(1); j++)
            {
                if(nodeMap[i, j] != null)
                {
                    nodes.Add(nodeMap[i, j], FindNeighbors(nodeMap,i, j));
                }
            }
        }

        potentialNodes = new PriorityQueue();
        examinedNodes = new PriorityQueue();
    }

    public List<Field> GetPath(Field startField, Field endField)
    {
        ClearAll();
        
        FindPathEnds(startField, endField);

        Node current = null;
        potentialNodes.Enqueue(start, GetRank(start));

        while(potentialNodes.Count != 0)
        {
            current = potentialNodes.Dequeue();
            if(current == end)
                return ConstructPath(current);
            foreach(Node neighbor in nodes[current])
            {
                if(neighbor != null && (!neighbor.field.IsOccupied() || neighbor.field.ElemOnField is Spider))
                {
                    if(neighbor.parent == null && neighbor != start)
                        neighbor.parent = current;
                    double nCost = GetRank(neighbor);
                    if(!examinedNodes.Contains(neighbor))
                    {
                        //if(potentialNodes.PeekBest() >= nCost)
                            potentialNodes.Enqueue(neighbor, nCost);
                    }
                }
            }
            examinedNodes.Enqueue(current, GetRank(current));
        }

        return null;
    }

    List<Field> ConstructPath(Node pathEnd)
    {
        List<Field> path = new List<Field>();
        path.Add(pathEnd.field);
        while(pathEnd.parent != null && pathEnd.parent != start)
        {
            path.Add(pathEnd.parent.field);
            pathEnd = pathEnd.parent;
        }
        return path;
    }
    double GetRank(Node node)
    {
        return DistanceFromStart(node) + CostHeuristic(node, end);
    }
    double DistanceFromStart(Node node)
    {
        if(start == node)
            return 0;
        return DistanceFromStart(node.parent) + 1;
    }
    double CostHeuristic(Node node1, Node node2)
    {
        double x = Mathf.Abs(node1.field.MapPosition.x - node2.field.MapPosition.x);
        double y = Mathf.Abs(node1.field.MapPosition.y - node2.field.MapPosition.y);

        return x + y;
    }
    void ClearAll()
    {
        potentialNodes.Clear();
        examinedNodes.Clear();
        foreach(var node in nodes)
        {
            node.Key.parent = null;
        }
        best = null;
        start = null;
        end = null;
    }
    void FindPathEnds(Field startField, Field endField)
    {
        foreach(Node node in nodes.Keys)
        { 
            if(start != null && end != null)
                break;
            if(node.field == startField)
                start = node;
            if(node.field == endField)
                end = node;
        }
    }
    Node[] FindNeighbors(Node[,] nodeMap, int i, int j)
    {
        Node[] neighbors = new Node[4];
        Node potentialNeighbor;
        if(InRange(nodeMap, i, j + 1) && (potentialNeighbor = nodeMap[i, j + 1]) != null)
            neighbors[0] = potentialNeighbor;
        if(InRange(nodeMap, i + 1, j) && (potentialNeighbor = nodeMap[i + 1, j]) != null)
            neighbors[1] = potentialNeighbor;
        if(InRange(nodeMap, i, j - 1) && (potentialNeighbor = nodeMap[i, j - 1]) != null)
            neighbors[2] = potentialNeighbor;
        if(InRange(nodeMap, i - 1, j) && (potentialNeighbor = nodeMap[i - 1, j]) != null)
            neighbors[3] = potentialNeighbor;

        return neighbors;
    }
    bool InRange(Node[,] nodeMap,int i, int j)
    {
        return i > 0 && i < nodeMap.GetLength(0) && j > 0 && j < nodeMap.GetLength(1);
    }
}

public class Node {
    public Field field;
    public Node parent;

    public Node(Field field)
    {
        this.field = field;
    }
    
}
