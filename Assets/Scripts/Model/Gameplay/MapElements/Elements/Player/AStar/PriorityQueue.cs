﻿using System;
using System.Collections.Generic;
using System.Linq;

public class PriorityQueue
{

    private List<Pair<Node, double>> elements = new List<Pair<Node, double>>();

    public int Count
    {
        get { return elements.Count; }
    }

    public void Enqueue(Node item, double priority)
    {
        elements.Add(new Pair<Node, double>(item, priority));
    }

    public Node Dequeue()
    {
        int bestIndex = 0;

        for(int i = 0; i < elements.Count; i++)
        {
            if(elements[i].second < elements[bestIndex].second)
            {
                bestIndex = i;
            }
        }

        Node bestItem = elements[bestIndex].first;
        elements.RemoveAt(bestIndex);
        return bestItem;
    }

    public double PeekBest()
    {
        int bestIndex = 0;

        for(int i = 0; i < elements.Count; i++)
        {
            if(elements[i].second < elements[bestIndex].second)
            {
                bestIndex = i;
            }
        }

        return elements.Count > 0 ? elements[bestIndex].second : double.MaxValue;
    }
    public void Clear()
    {
        elements.Clear();
    }

    public bool Contains(Node neighbor)
    {
        return elements.Exists(x => x.first == neighbor);
    }
}