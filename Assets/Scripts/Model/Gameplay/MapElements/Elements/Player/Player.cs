﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MapElement, IMoveable {

    [SerializeField] float fieldTravelTime = 0.5f;
    
    InteractionHandler interactionHandler;
    PlayerMovement movement;
    ElementAnimator animator;

    bool isDead = false;

    void Awake()
    {
        interactionHandler = new InteractionHandler(this);
        movement = GetComponent<PlayerMovement>();
        animator = GetComponent<ElementAnimator>();
        
    }

    public bool CanMove(Direction dir)
    {
        throw new NotImplementedException();
    }

    public bool canMove(Field field)
    {
        //return movement.validatePath(field);
        throw new NotImplementedException();
    }

    public void Move(Direction dir)
    {
        movement.Move(dir);
    }

    public void Move(Field field)
    {
        Move(field.DirectionFromElement(this));
    }

    public void InteractWith(IInteractable elem)
    {
        interactionHandler.InteractWith(elem);
    }

    public override void OnInteractionEnd()
    {
        interactionHandler.OnInteractionEnd();
    }

    public void Death()
    {
        if(isDead)
            return;

        isDead = true;
        GetComponent<PlayerController>().enabled = false;
        interactionHandler.actionsQueue.Clear();
        animator.SetState(AnimState.playerDeath);
        GameplayManager.Instance.OnPlayerDeath();
    }
    public void Ressurect()
    {
        isDead = false;
        interactionHandler.busy = false;
        GetComponent<PlayerController>().enabled = true;
        animator.SetState(AnimState.idle);
    }

    public float FieldTravelSpeed
    {
        get { return fieldTravelTime; }
        set { fieldTravelTime = value; }
    }
    public bool Busy
    {
        get { return interactionHandler.busy; }
        set { interactionHandler.busy = value; }
    }
    public bool IsDead
    {
        get { return isDead; }
    }
    public ElementAnimator Animator
    {
        get { return animator; }
    }
    public PlayerMovement Movement
    {
        get { return movement; }
    }
}

