﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : ElementMovement
{
    AStarSearch pathFinder;

    protected override void Awake()
    {
        base.Awake();
    }

    public void InitPathfinding()
    {
        pathFinder = new AStarSearch(); 
    }

    public List<Field> GetPathToField(MapElement startElem, Field dest)
    {
        return pathFinder.GetPath(LevelMap.Instance.GetElement<Field>(startElem.MapPosition), dest);
    }

}
    

