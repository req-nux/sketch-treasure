﻿using System.Collections.Generic;
using UnityEngine;

public class InteractionHandler {

    public bool busy;
    public Queue<IInteractable> actionsQueue;
    int actionQueueCapacity = 30;

    Player player;
    IInteractable currentInteractionElem;

    public InteractionHandler(Player player)
    {
        this.player = player;
        actionsQueue = new Queue<IInteractable>();
    }

    public void InteractWith(IInteractable elem)
    {
        if(player.IsDead)
            return;
        actionsQueue.Clear();

        Debug.Log("interact wih " + elem.ToString());
        if(elem is Field && !elem.InInteractionRange())
            HandleFieldInteraction((Field)elem);
        else if(CanBeAddedToQueue(elem))
            actionsQueue.Enqueue(elem);
        else
        {
            //if elem cant be added to queue and is field
            // check if there are any interactable elems on this field
            if(elem is Field && ((Field)elem).IsOccupied() && CanBeAddedToQueue(((Field)elem).ElemOnField))
                actionsQueue.Enqueue(elem);
        }

        if(!busy)
        {
            var e = GetNextInteractableElemFromQueue();
            if(e != null)
                StartInteraction(e);
        }
    }

    public void OnInteractionEnd()
    {
        if(player.IsDead)
            return;

        var elem = GetNextInteractableElemFromQueue();
        if(elem != null)
            StartInteraction(elem);
        else
        {
            busy = false;
            currentInteractionElem = null;
            player.Animator.SetState(AnimState.idle);
        }
    }

    void StartInteraction(IInteractable elem)
    {
        busy = true;
        currentInteractionElem = elem;
        elem.Interact();
    }

    bool CanBeAddedToQueue(IInteractable elem)
    {
        return actionsQueue.Count < actionQueueCapacity
            && !actionsQueue.Contains(elem)
            && (currentInteractionElem != elem || currentInteractionElem is Rock);
    }

    IInteractable GetNextInteractableElemFromQueue()
    {
        while(actionsQueue.Count > 0 && !actionsQueue.Peek().CanInteract())
            actionsQueue.Dequeue();
        return actionsQueue.Count > 0 ? actionsQueue.Dequeue() : null;
    }

    void HandleFieldInteraction(Field field)
    {
        bool queueContainsField = false;
        Field lastAddedField = null;
        foreach(var elem in actionsQueue)
            if(elem is Field)
            {
                queueContainsField = true;
                lastAddedField = (Field)elem;
            }

        List<Field> path = player.Movement.GetPathToField(LevelMap.Player, field);
        if(path != null)
        {
            for(int i = path.Count-1; i >= 0; i--)
            {
                if(CanBeAddedToQueue(path[i]))
                    actionsQueue.Enqueue(path[i]);
            }
        }
        /*
        if(queueContainsField && player.Movement.ValidatePath(lastAddedField, field))
        {
            foreach(var f in player.Movement.GetPathToField(lastAddedField,field))
                if(CanBeAddedToQueue(f))
                    actionsQueue.Enqueue(f);
        }
        else if(!queueContainsField && player.Movement.ValidatePath(LevelMap.Player, field))
        {
            foreach(var f in player.Movement.GetPathToField(LevelMap.Player, field))
                if(CanBeAddedToQueue(f))
                    actionsQueue.Enqueue(f);
        }
        */
    }

}
