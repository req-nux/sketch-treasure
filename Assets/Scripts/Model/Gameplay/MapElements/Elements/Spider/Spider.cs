﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : MapElement, IMoveable, IConfigurable
{
    [SerializeField] float fieldTravelTime = 0.6f;
    [SerializeField] List<Vector2> pathPoints;
    [SerializeField] CommonEngine.ScriptableObjects.AudioObject attackSound;

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.collider.gameObject.tag == "Player" && !col.collider.GetComponent<Player>().IsDead)
        {
            SoundManager.Instance.PlayFX(attackSound);
            col.collider.GetComponent<Player>().Death();
        }
    }

    public bool CanMove(Direction dir)
    {
        if(dir == Direction.Undefined)
            return false;

        Vector2 destMapPos = MapPosition + Formatter.directionToVec2(dir);
        if(LevelMap.Player.MapPosition == destMapPos)
            return true;
        return LevelMap.Instance.CanMove(destMapPos);
    }

    public ElementConfig GetConfig()
    {
        return new SpiderConfig(transform.position, pathPoints);
    }

    public void Move(Field field)
    {
        throw new NotImplementedException();
    }

    public void Move(Direction dir)
    {
        throw new NotImplementedException();
    }

    public List<Vector2> PathPoints
    {
        get { return pathPoints; }
        set { pathPoints = value; }
    }
    public float FieldTravelSpeed
    {
        get { return fieldTravelTime; }
        set { fieldTravelTime = value; }
    }
}
