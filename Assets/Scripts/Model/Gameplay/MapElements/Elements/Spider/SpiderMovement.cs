﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderMovement : ElementMovement {

    [SerializeField] bool active;
    List<Field> pathFields;
    int step = 0;
    Direction stepDir;
    Field destinationField;

    protected override void Awake () {
        base.Awake();
        pathFields = createPathFields();
    }
    
    void Update () {
        if(active && !isMoving)
        {
            destinationField = GetNextFieldToGo();
            if(destinationField == null)
                return;

            stepDir = destinationField.DirectionFromElement(elem);
            if(elem.MapPosition != destinationField.MapPosition && ((IMoveable)elem).CanMove(stepDir))
            {
                Move(stepDir);
                anim.HandleSpiderMovementDirection(stepDir);
            }
        }
    }

    Field GetNextFieldToGo()
    {
        Direction dir;
        for(int i = step; i < pathFields.Count; i++)
        {
            dir = pathFields[i].DirectionFromElement(elem);
            if(elem.MapPosition != pathFields[i].MapPosition && ((IMoveable)elem).CanMove(dir))
            {
                step = i;
                return pathFields[i];
            }
        }
        for(int i = 0; i < step; i++)
        {
            dir = pathFields[i].DirectionFromElement(elem);
            if(elem.MapPosition != pathFields[i].MapPosition && ((IMoveable)elem).CanMove(dir))
            {
                step = i;
                return pathFields[i];
            }
        }
        return null;
    }

    List<Field> createPathFields()
    {
        List<Vector2> pathPoints = ((Spider)elem).PathPoints;
        List<Field> path = new List<Field>(pathPoints.Count);
        foreach(var p in pathPoints)
            path.Add(LevelMap.Instance.GetElement<Field>(p));

        return path;
    }

    public bool Active
    {
        get { return active; }
        set { active = value; }
    }
}
