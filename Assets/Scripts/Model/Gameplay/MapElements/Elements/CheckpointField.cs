﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointField : Field {

    ElementAnimator animator;
    GameplayManager gameplayManager;
    GameObject checkpointMark;

    void Awake()
    {
        gameplayManager = GameObject.FindObjectOfType<GameplayManager>();
        checkpointMark = transform.Find("checkpointMark").gameObject;
        animator = GetComponent<ElementAnimator>();
    }

    public void setCheckpointActive(bool active)
    {
        checkpointMark.SetActive(active);
        animator.HandleCheckpointFieldStateChange(active);
    }

    public override void OnInteractionEnd()
    {
        gameplayManager.RegisterCheckpoint(this);
        LevelMap.Player.OnInteractionEnd();
    }
}
