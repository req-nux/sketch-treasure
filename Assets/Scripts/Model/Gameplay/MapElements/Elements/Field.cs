﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MapElement {

    protected MapElement elemOnField;

    public override void Interact()
    {
        if(CanInteract())
        {
            isDuringInteraction = true;
            LevelMap.Player.Animator.HandleMovementDirection(DirectionFromElement(LevelMap.Player));
            LevelMap.Player.Move(this);
            StartCoroutine(WaitForInteractionEnd());
        }
        else
            OnInteractionEnd();
    }

    IEnumerator WaitForInteractionEnd()
    {
        yield return new WaitUntil(() => !LevelMap.Player.Movement.IsMoving);
        OnInteractionEnd();
    }

    public override void OnInteractionEnd()
    {
        isDuringInteraction = false;
        LevelMap.Player.OnInteractionEnd();
    }

    public override bool CanInteract()
    {
        return InInteractionRange() && !IsOccupied();
    }

    public virtual bool IsOccupied()
    {
        return elemOnField != null;
    }

    public override string ToString()
    {
        if(elemOnField != null)
            return base.ToString() + " occupied by " + elemOnField.ToString();
        return base.ToString();
    }

    public virtual MapElement ElemOnField
    {
        get { return elemOnField; }
        set { elemOnField = value; }
    }

}
