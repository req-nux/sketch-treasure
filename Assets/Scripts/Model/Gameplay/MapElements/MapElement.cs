﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MapElement : MonoBehaviour , IInteractable {

    protected bool isDuringInteraction;

    [SerializeField] bool occupiesField;
    Vector2 mapPosition;

    public virtual void Interact()
    {
        OnInteractionEnd();
    }
    public virtual void OnInteractionEnd()
    {
        LevelMap.Player.OnInteractionEnd();
    }

    public virtual bool CanInteract()
    {
        return InInteractionRange();
    }

    public virtual bool InInteractionRange()
    {
        return (mapPosition - LevelMap.Player.mapPosition).magnitude <= LevelMap.FieldSize;
    }

    public Direction DirectionFromElement(MapElement elem)
    {
        if(elem.mapPosition.x != X && elem.mapPosition.y != Y)
            return Direction.Undefined;

        if(elem.mapPosition.x == X)
            return elem.mapPosition.y > Y ? Direction.Down : Direction.Up;
        else
            return elem.mapPosition.x > X ? Direction.Left : Direction.Right;
    }
    public bool IsDuringInteraction()
    {
        return isDuringInteraction;
    }
    public bool OccupiesField
    {
        get { return occupiesField; }
    }
    public Vector2 MapPosition
    {
        get { return mapPosition; }
        set { mapPosition = value; }
    }
    public int X { get { return (int)mapPosition.x; }}
    public int Y { get { return (int)mapPosition.y; } }

    public override string ToString()
    {
        return gameObject.name + " " + mapPosition;
    }

}
