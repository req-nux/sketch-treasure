﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Holds 2d array, that contains fields of level
/// Other MapElements are kept inside fields
/// </summary>
public class LevelMap : CommonEngine.Utils.MonoBehaviourSingleton<LevelMap> {

    public const float FieldSize = 1f;
    static Player player;

    Field[,] map;

    public delegate void OnElemMoved();
    public static event OnElemMoved OnElementMoved;

    void Awake()
    {
        player = GetComponentInChildren<Player>();
    }

    public static Player Player
    {
        get
        {
            if(player == null)
            {
                player = GameObject.FindObjectOfType<Player>();
                if(player == null)
                    Debug.LogError("Cant find Player object in the scene!");
            }
            return player;
        }
    }

    public void InitSize(Vector2 size)
    {
        map = new Field[(int)size.x,(int)size.y];
    }

    public void MoveElement(MapElement elem, Vector2 newPos)
    {
        if(GetElement<Field>(newPos) == null)
            Debug.LogError("There is no field on newPos : " + newPos);

        if(elem.OccupiesField)
        {
            GetElement<Field>(elem.MapPosition).ElemOnField = null;
            GetElement<Field>(newPos).ElemOnField = elem;
        }
        elem.MapPosition = newPos;

        if(OnElementMoved != null)
            OnElementMoved();
    }

    public T GetElement<T>(Vector2 pos) where T : MapElement
    {
        if(pos.x < 0 || pos.x > map.GetLength(0) || pos.y < 0 || pos.y > map.GetLength(1))
            return null;

        Field field = map[(int)pos.x, (int)pos.y];

        if(!(field is T) && field != null && !(field.ElemOnField is T))
            return null;

        if(!(field is T) && field != null)
            return (T)field.ElemOnField;
        return (T)(MapElement)field;
    }

    public bool CanMove(Vector2 pos)
    {
        return map[(int)pos.x, (int)pos.y] != null && !map[(int)pos.x, (int)pos.y].IsOccupied();
    }

    public Field[,] Map
    {
        get { return map; }
    }

}
