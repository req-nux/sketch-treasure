﻿
public enum Direction {
    Undefined,
    Up,
    Right,
    Down,
    Left
}
