﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents single level on labirynth map
/// </summary>
public class MapSegment : MonoBehaviour {

    [SerializeField] LevelConfig levelObject;
    [SerializeField] List<MapSegment> unlockedSegments;
    bool unlocked;

    public void Init()
    {
        GetComponent<SegmentButton>().Init();
    }

    public LevelConfig Level
    {
        get { return levelObject; }
    }

    public List<int> UnlockedSegmentsIDs
    {
        get
        {
            List<int> ids = new List<int>();
            foreach(MapSegment ms in unlockedSegments)
                ids.Add(ms.Level.ID);
            return ids;
        }
    }

    public bool Unlocked
    {
        get { return unlocked; }
        set
        {
            unlocked = value;
            GetComponent<SegmentButton>().SetUnlocked(unlocked);
        }
    }

}
