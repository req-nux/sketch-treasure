﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyRequirement : MonoBehaviour {

    [SerializeField] int keyFieldId;

    public bool PlayerHasKey()
    {
        return PlayerPrefs.HasKey(string.Format(PersistentDataManager.keyDataString, keyFieldId));
    }
}
