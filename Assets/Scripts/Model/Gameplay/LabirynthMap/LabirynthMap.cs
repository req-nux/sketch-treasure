﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabirynthMap : MonoBehaviour {

    public const int SEGMENTS_COUNT = 22;

    MapSegment[] segments;

    void Start () {
        segments = transform.GetComponentsInChildren<MapSegment>();
        if(segments.Length != SEGMENTS_COUNT)
            Debug.LogWarning("Wrong number of segments in map!");
        foreach(var segment in segments)
            segment.Init();
        LoadStateFromFile();
    }
    
    void LoadStateFromFile()
    {
        LabirynthMapState state = PersistentDataManager.loadLabirynthMapState();
        for(int i = 0; i < segments.Length; i++)
        {
            MapSegmentState segmentStateByID = state.getSegmentStateById(segments[i].Level.ID);
            if(segmentStateByID == null)
            {
                Debug.LogError("LoadStateFromFile: Can't load segment from file. ID = " + segments[i].Level.ID);
            }
            segments[i].Unlocked = segmentStateByID.unlocked;
        }
        GetComponent<LabirynthMapView>().OnStateLoad();
    }

    public MapSegment[] MapSegments
    {
        get { return segments; }
    }
}
