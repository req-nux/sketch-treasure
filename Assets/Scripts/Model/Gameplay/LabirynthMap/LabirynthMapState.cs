﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents state of labirynth map. Contains segment id and information if its unlocked.
/// Segments infos are not sorted.
/// </summary>
[System.Serializable]
public class LabirynthMapState {

    MapSegmentState[] state;

    public LabirynthMapState()
    {
        state = new MapSegmentState[LabirynthMap.SEGMENTS_COUNT];
    }

    public void initEmpty()
    {
        for(int i = 0; i < state.Length; i++)
            state[i] = new MapSegmentState(i+1, false, false);
        state[0].unlocked = true;
    }

    public MapSegmentState getSegmentStateById(int id)
    {
        foreach(MapSegmentState s in state)
            if(s.id == id)
                return s;
        Debug.LogError("No matching segment ID in LabirynthMapState! Searched ID: " + id);
        return null;
    }

    public void setSegmentUnlocked(int id)
    {
        getSegmentStateById(id).unlocked = true;
    }

    public void setSegmentCompleted(int id)
    {
        getSegmentStateById(id).completed = true;
        SegmentUnlockingDependencies sud = Resources.Load<SegmentUnlockingDependencies>("SegmentUnlockingDependencies");

        var segmentDependencies = System.Array.Find<SegmentDependencies>(sud.Dependecies, x => x.segmentId == id);
        if(segmentDependencies != null)
        {
            foreach(int unlockedId in segmentDependencies.unlockingIds)
                setSegmentUnlocked(unlockedId);
        }
        else Debug.LogError("Can't find SegmentUnlockingDependencies for segment id = " + id);
    }
}