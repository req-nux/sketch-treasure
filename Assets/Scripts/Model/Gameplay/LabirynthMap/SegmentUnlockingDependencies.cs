﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable object that contais info about what segments should be unlocked after completing certain segment
/// </summary>
public class SegmentUnlockingDependencies : ScriptableObject {

    //<completed segment id, list of unlocked ids>
    public SegmentDependencies[] Dependecies;

    public override string ToString()
    {
        string s = "";
        foreach(var d in Dependecies)
        {
            s += "( " + d.segmentId + "|";
            foreach(var id in d.unlockingIds)
                s += "id ";
            s += ")";
        }
        return s;
    }

}

[System.Serializable]
public class SegmentDependencies
{
    public int segmentId;
    public List<int> unlockingIds;

    public SegmentDependencies(int id, List<int> ids)
    {
        segmentId = id;
        unlockingIds = ids;
    }
}
