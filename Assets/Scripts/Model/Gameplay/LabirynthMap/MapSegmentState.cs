﻿
[System.Serializable]
public class MapSegmentState
{

    public int id;
    public bool unlocked;
    public bool completed;

    public MapSegmentState(int id, bool unlocked, bool completed)
    {
        this.id = id;
        this.unlocked = unlocked;
        this.completed = completed;
    }
}