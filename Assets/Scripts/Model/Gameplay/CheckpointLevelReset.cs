﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointLevelReset : MonoBehaviour {

    public delegate void OnLevelReset();
    public static event OnLevelReset onLevelReset;

    public static bool isDuringLevelReset = false;

    public void resetLevelToState(CheckpointLevelState state)
    {
        isDuringLevelReset = true;
        if(onLevelReset != null)
            onLevelReset();

        LevelMap.Instance.MoveElement(LevelMap.Player, state.playerPos);
        setTransformBasedOnMapPosition(LevelMap.Player);

        for(int i = 0; i < state.spiders.Length; i++)
        {
            LevelMap.Instance.MoveElement(state.spiders[i], state.spidersPos[i].Value);
            setTransformBasedOnMapPosition(state.spiders[i]);
        }
        for(int i = 0; i < state.rocks.Length; i++)
        {
            LevelMap.Instance.MoveElement(state.rocks[i], state.rocksPos[i].Value);
            setTransformBasedOnMapPosition(state.rocks[i]);
        }
        for(int i = 0; i < state.levers.Length; i++)
        {
            if(state.levers[i].Active != state.leversState[i].Value)
                state.levers[i].changeStateImmediate();
        }
        for(int i = 0; i < state.circleLevers.Length; i++)
        {
            if(state.circleLevers[i].State != state.circleLeversState[i].Value)
                state.circleLevers[i].setStateImmediate(state.circleLeversState[i].Value);
        }
        for(int i = 0; i < state.bars.Length; i++)
        {
            if(state.bars[i].Opened != state.barsState[i].Value)
                state.bars[i].changeStateImmediate();
        }

        LevelMap.Player.Ressurect();
        isDuringLevelReset = false;
    }

    void setTransformBasedOnMapPosition(MapElement elem)
    {
        elem.transform.position = new Vector3
            (
                elem.MapPosition.x - LevelBuilder.mapOffset.x,
                elem.MapPosition.y - LevelBuilder.mapOffset.y,
                elem.transform.position.z
            );
    }
}
