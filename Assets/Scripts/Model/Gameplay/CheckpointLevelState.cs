﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointLevelState {

    public Spider[] spiders;
    public Rock[] rocks;
    public Lever[] levers;
    public CircleLever[] circleLevers;
    public Bars[] bars;

    public Vector2 playerPos;
    public KeyValuePair<Spider, Vector2>[] spidersPos;
    public KeyValuePair<Rock, Vector2>[] rocksPos;
    public KeyValuePair<Lever, bool>[] leversState;
    public KeyValuePair<CircleLever, int>[] circleLeversState;
    public KeyValuePair<Bars, bool>[] barsState;

    public CheckpointLevelState(Transform elementsContainer, Transform spidersContainer)
    {
        spiders = spidersContainer.GetComponentsInChildren<Spider>();
        Debug.Log("spiders.length" + spiders.Length);
        rocks = elementsContainer.GetComponentsInChildren<Rock>();
        levers = elementsContainer.GetComponentsInChildren<Lever>();
        circleLevers = elementsContainer.GetComponentsInChildren<CircleLever>();
        bars = elementsContainer.GetComponentsInChildren<Bars>();

        spidersPos = new KeyValuePair<Spider, Vector2>[spiders.Length];
        rocksPos = new KeyValuePair<Rock, Vector2>[rocks.Length];
        leversState = new KeyValuePair<Lever, bool>[levers.Length];
        circleLeversState = new KeyValuePair<CircleLever, int>[circleLevers.Length];
        barsState = new KeyValuePair<Bars, bool>[bars.Length];
    }

    public void saveState()
    {
        playerPos = LevelMap.Player.MapPosition;
        Debug.Log("Saved player pos " + playerPos);
        for(int i = 0; i < spiders.Length; i++)
        {
            spidersPos[i] = new KeyValuePair<Spider, Vector2>(spiders[i], spiders[i].MapPosition);
        }
        for(int i = 0; i < rocks.Length; i++)
        {
            rocksPos[i] = new KeyValuePair<Rock, Vector2>(rocks[i], rocks[i].MapPosition);
        }
        for(int i = 0; i < levers.Length; i++)
        {
            leversState[i] = new KeyValuePair<Lever, bool>(levers[i], levers[i].Active);
        }
        for(int i = 0; i < circleLevers.Length; i++)
        {
            circleLeversState[i] = new KeyValuePair<CircleLever, int>(circleLevers[i], circleLevers[i].State);
        }
        for(int i = 0; i < bars.Length; i++)
        {
            barsState[i] = new KeyValuePair<Bars, bool>(bars[i], bars[i].Opened);
        }
    }
}
