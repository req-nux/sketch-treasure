﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimState {

    idle = 0,
    moveRight = 1,
    moveLeft,
    moveUp,
    moveDown,
    pushRockRight = 5,
    pushRockLeft,
    pushRockUp,
    pushRockDown,
    barsOpenUp = 9,
    barsCloseUp,
    barsOpenRight,
    barsCloseRight,
    leverLeft = 13,
    leverRight,
    playerDeath = 15,
    buttonFieldPressed = 16,
    buttonFieldReleased,
    checkpointFieldActivation = 18,
    checkpointFieldDeactivation,
    leverFromLeft_Right = 20,
    leverFromLeft_Left,
    leverFromRight_Right,
    leverFromRight_Left,
    leverFromBottom_Right,
    leverFromBottom_Left,
    leverFromTop_Right,
    leverFromTop_Left,
    leverFromDown_Right,
    leverFromDown_Left,
    treasurePickUp,

    UNDEFINED

}
