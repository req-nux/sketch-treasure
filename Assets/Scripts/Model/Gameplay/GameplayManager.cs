﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : CommonEngine.Utils.MonoBehaviourSingleton<GameplayManager> {

    [SerializeField] GameMenu gameMenu;
    [SerializeField] float onPlayerDeathDelay;
    CheckpointField recentCheckpoint;
    CheckpointLevelState checkpointLevelState;

    /*void Start()
    {
        checkpointLevelState = new CheckpointLevelState(GameObject.Find("Elements").transform,
            GameObject.Find("Spiders").transform);

    }*/

    public void RegisterCheckpoint(CheckpointField checkPoint)
    {
        if(recentCheckpoint != null)
            recentCheckpoint.setCheckpointActive(false);
        checkPoint.setCheckpointActive(true);
        recentCheckpoint = checkPoint;
        Debug.Log("RegisterCheckpoint: Level saved");
        checkpointLevelState.saveState();
    }

    public void OnLevelComplete()
    {
        LevelMap.Player.Busy = true;
        gameMenu.HideMenuButton();
        SaveLevelCompletion();
        GameManager.Instance.SceneManager.LoadScene("MainMenu", SceneChangeMode.OnLevelComplete);
    }

    public void OnPlayerDeath()
    {
        StartCoroutine(PlayerDeath());
    }

    IEnumerator PlayerDeath()
    {
        yield return new WaitForSeconds(onPlayerDeathDelay);
        gameMenu.HideMenuButton();
        if(recentCheckpoint != null)
            GetComponent<CheckpointLevelReset>().resetLevelToState(checkpointLevelState);
        else
            GetComponent<GameSceneManager>().LoadScene("Gameplay", SceneChangeMode.Default);
    }

    void SaveLevelCompletion()
    {
        LabirynthMapState lms = PersistentDataManager.loadLabirynthMapState();
        lms.setSegmentCompleted(GameManager.Instance.LoadedLevel.ID);
        PersistentDataManager.saveLabirynthMapState(lms);
        Debug.Log("LabirynthMapState saved!");
    }

}