﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum SceneChangeMode
{
    Default,
    OnLevelComplete,
    LeaveChamber
}

public class GameSceneManager : MonoBehaviour {

    static SceneChangeMode sceneChangeMode = SceneChangeMode.Default;

    [SerializeField] ScrollAnimator scrollAnimator;
    string sceneNameToLoad;

    public void LoadScene(string name, SceneChangeMode mode)
    {
        sceneChangeMode = mode;
        sceneNameToLoad = name;
        scrollAnimator.CloseScroll(LoadScene);
    }

    public void RestartCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void LoadScene()
    {
        SceneManager.LoadScene(sceneNameToLoad);
    }

    public SceneChangeMode SceneChangeMode
    {
        get { return sceneChangeMode; }
    }
}
