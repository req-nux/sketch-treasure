﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class PersistentDataManager : MonoBehaviour {

    public const string keyDataString = "key{0}Found";
    const string lmsDataPath = "/LMS.dat";

    public static void saveLabirynthMapState(LabirynthMapState state)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + lmsDataPath);
        bf.Serialize(file, state);
        file.Close();
        Debug.Log("saved in " + Application.persistentDataPath + lmsDataPath);
    }

    public static LabirynthMapState loadLabirynthMapState()
    {
        if(File.Exists(Application.persistentDataPath + lmsDataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + lmsDataPath, FileMode.Open);
            LabirynthMapState state = (LabirynthMapState)bf.Deserialize(file);
            file.Close();
            return state;
        }
        LabirynthMapState emptyState = new LabirynthMapState();
        emptyState.initEmpty();
        return emptyState;
    }
}
