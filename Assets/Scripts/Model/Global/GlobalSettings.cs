﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalSettings {

    public static LanguageID GetLanguage()
    {
        return (LanguageID)PlayerPrefs.GetInt("language", (int)LanguageID.Polish);
    }
    public static void SetLanguage(LanguageID id)
    {
        PlayerPrefs.SetInt("language", (int)id);
    }
}
