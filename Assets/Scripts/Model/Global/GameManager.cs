﻿using UnityEngine;

public class GameManager {

    static GameManager instance;
    static GameSceneManager sceneManager;
    MapSegment selectedSegment;
    LevelConfig loadedLevel;

    GameManager() {}

    public static GameManager Instance
    {
        get
        {
            if(instance == null)
                instance = new GameManager();
            return instance;
        }
    }

    public MapSegment SelectedSegment
    {
        get { return selectedSegment; }
        set
        {
            selectedSegment = value;
            loadedLevel = selectedSegment == null ? null : selectedSegment.Level;
            if(loadedLevel != null)
                Debug.Log("loadedLevel ID " + loadedLevel.ID);
        }
    }

    public GameSceneManager SceneManager
    {
        get
        {
            if(sceneManager == null)
                sceneManager = GameObject.FindObjectOfType<GameSceneManager>();
            return sceneManager;
        }
    }


    public LevelConfig LoadedLevel
    {
        get { return loadedLevel; }
    }

}
