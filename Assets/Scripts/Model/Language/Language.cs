﻿using System.Collections.Generic;

public enum LanguageID
{
    Polish,
    English
}

/// <summary>
/// Base class for every language in game.
/// Contains dictionary <TextID, string>
/// </summary>
public abstract class Language {

    protected Dictionary<TextID, string> texts;

    public Dictionary<TextID, string> Texts
    {
        get { return texts; }
    }
}
