﻿
using UnityEngine;
/// <summary>
/// Singleton used to deliver texts translations in language set in game settings,
/// by its text id.
/// </summary>
public class LanguageLibrary {

    public delegate void OnLanguageChange();
    public static event OnLanguageChange onLanguageChange;

    static LanguageLibrary instance;
    Language currentLanguage;

    LanguageLibrary()
    {
        setLanguage(GlobalSettings.GetLanguage());
    }

    public static LanguageLibrary Instance
    {
        get
        {
            if(instance == null)
                instance = new LanguageLibrary();
            return instance;
        }
    }

    public string getText(TextID id)
    {
        return currentLanguage.Texts[id];
    }

    public void setLanguage(LanguageID id)
    {
        switch(id)
        {
            case LanguageID.Polish : currentLanguage = new Polish(); break;
            case LanguageID.English : currentLanguage = new English(); break;
        }
        GlobalSettings.SetLanguage(id);
        if(onLanguageChange != null)
            onLanguageChange();
    }

}
