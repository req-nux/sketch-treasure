﻿using System.Collections;
using System.Collections.Generic;

public class English : Language {

    public English()
    {
        texts = new Dictionary<TextID, string>()
        {
            { TextID.SketchTreasure,"Sketch Treasure" },
            { TextID.Start,"Start" },
            { TextID.Settings,"Settings" },
            { TextID.Back,"Back" },
            { TextID.EnterChamber,"Enter Chamber" },
            { TextID.LabirynthMap,"Labirynth Map" },
            { TextID.TryAgain,"Try again" },
            { TextID.LeaveChamber,"Leave chamber" },
            { TextID.Language,"Language" },
            { TextID.Language_Polski,"Polski" },
            { TextID.Language_English,"English" },
            
        };
    }

}
