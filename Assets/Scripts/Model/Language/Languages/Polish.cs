﻿using System.Collections;
using System.Collections.Generic;

public class Polish : Language {

    public Polish()
    {
        texts = new Dictionary<TextID, string>()
        {
            { TextID.SketchTreasure,"Sketch Treasure" },
            { TextID.Start,"Start" },
            { TextID.Settings,"Ustawienia" },
            { TextID.Back,"Wróć" },
            { TextID.EnterChamber,"Wejdź do komnaty" },
            { TextID.LabirynthMap,"Mapa Labiryntu" },
            { TextID.TryAgain,"Spróbuj ponownie" },
            { TextID.LeaveChamber,"Opuść komnatę" },
            { TextID.Language,"Język" },
            { TextID.Language_Polski,"Polski" },
            { TextID.Language_English,"English" },

        };
    }

}
