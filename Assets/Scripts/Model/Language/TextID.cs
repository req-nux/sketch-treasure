﻿
/// <summary>
/// Represents all texts existing in the game
/// </summary>
public enum TextID
{
    SketchTreasure,
    Start,
    Settings,
    Back,
    EnterChamber,
    LabirynthMap,
    TryAgain,
    LeaveChamber,
    Language,
    Language_Polski,
    Language_English

}
