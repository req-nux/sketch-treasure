﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Formatter{

    public static Vector2 vector3To2(Vector3 v)
    {
        return new Vector2(v.x, v.y);
    }

    public static Vector2 directionToVec2(Direction dir)
    {
        switch(dir)
        {
            case Direction.Up: return Vector2.up;
            case Direction.Down: return Vector2.down;
            case Direction.Left: return Vector2.left;
            default: return Vector2.right;
        }
    }

}
