﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Configuration of whole level.
/// Saved as asset in Resources/Levels.
/// Each file describes one level.
/// </summary>
public class LevelConfig : ScriptableObject {

    public int ID;
    public string devDescription;
    public Vector2 mapSize;
    public Vector2 startCameraPos;
    public Vector2 startPos;
    public Vector2 startDoor;
    public Vector2 door;
    public List<Vector2> treasureHolder;

    public List<Vector2> fields;
    public List<Vector2> walls;
    public List<Vector2> rocks;
    public List<Vector2> evilLasers;

    public List<BarsConfig> bars;
    public List<MechanismConfig> levers;
    public List<CircleLeverConfig> circleLevers;
    public List<MechanismConfig> buttonFields;
    public List<WebConfig> webs;

    public List<Vector2> waterFields;
    public List<Vector2> checkpointFields;
    public List<Vector2> chests;

    public List<SpiderConfig> spiders;



}
