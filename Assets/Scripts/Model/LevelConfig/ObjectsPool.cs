﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsPool : MonoBehaviour {

    [SerializeField] Bars bars;
    [SerializeField] Lever lever;
    [SerializeField] CircleLever circleLever;
    [SerializeField] ButtonField buttonField;
    [SerializeField] Rock rock;
    [SerializeField] Spider spider;
    [SerializeField] WaterField waterField;
    [SerializeField] CheckpointField checkpointField;
    [SerializeField] Key chest;
    [SerializeField] EvilLaserSource evilLaser;
    [SerializeField] TreasureHolder treasureHolder;

    [SerializeField] Field fieldPrefab;
    [SerializeField] Wall wallPrefab;
    [SerializeField] Web web;
    [SerializeField] GameObject highlightPrefab;

    [SerializeField] int fieldPoolSize;
    [SerializeField] int wallPoolSize;
    [SerializeField] int highlightPoolSize;

    [SerializeField] List<Field> fieldPool;
    [SerializeField] List<Wall> wallPool;
    [SerializeField] List<GameObject> highlightPool;

    void Awake()
    {
        fieldPool = new List<Field>();
        for(int i = 0; i < fieldPoolSize; i++)
            addFieldToPool();

        wallPool = new List<Wall>();
        for(int i = 0; i < wallPoolSize; i++)
            addWallToPool();

        highlightPool = new List<GameObject>();
        for(int i = 0; i < highlightPoolSize; i++)
            addHighlightToPool();

    }

    public Field getField()
    {
        for(int i = 0; i < fieldPool.Count; i++)
        {
            if(fieldPool[i] != null && !fieldPool[i].gameObject.activeInHierarchy)
                return fieldPool[i];
        }
        return addFieldToPool();
    }

    Field addFieldToPool()
    {
        Field obj = (Field)Instantiate(fieldPrefab);
        obj.gameObject.SetActive(false);
        fieldPool.Add(obj);
        return obj;
    }

    public Wall getWall()
    {
        for(int i = 0; i < wallPool.Count; i++)
        {
            if(wallPool[i] != null && !wallPool[i].gameObject.activeInHierarchy)
                return wallPool[i];
        }
        return addWallToPool();
    }

    Wall addWallToPool()
    {
        Wall obj = (Wall)Instantiate(wallPrefab);
        obj.gameObject.SetActive(false);
        wallPool.Add(obj);
        return obj;
    }

    public GameObject getHighlight()
    {
        for(int i = 0; i < highlightPool.Count; i++)
        {
            if(highlightPool[i] != null && !highlightPool[i].gameObject.activeInHierarchy)
                return highlightPool[i];
        }
        return addHighlightToPool();
    }

    GameObject addHighlightToPool()
    {
        GameObject obj = (GameObject)Instantiate(highlightPrefab);
        obj.gameObject.SetActive(false);
        highlightPool.Add(obj);
        return obj;
    }

    public Rock getRock()
    {
        return (Rock)Instantiate(rock);
    }

    public Key getChest()
    {
        return (Key)Instantiate(chest);
    }

    public Lever getLever()
    {
        return (Lever)Instantiate(lever);
    }
    public CircleLever getCircleLever()
    {
        return (CircleLever)Instantiate(circleLever);
    }

    public ButtonField getButtonField()
    {
        return (ButtonField)Instantiate(buttonField);
    }

    public Bars getBars()
    {
        return (Bars)Instantiate(bars);
    }

    public WaterField GetWaterField()
    {
        return (WaterField)Instantiate(waterField);
    }

    public EvilLaserSource GetEvilLaser()
    {
        return (EvilLaserSource)Instantiate(evilLaser);
    }

    public Spider getSpider()
    {
        return (Spider)Instantiate(spider);
    }
    public CheckpointField getCheckpointField()
    {
        return (CheckpointField)Instantiate(checkpointField);
    }

    public Web GetWeb()
    {
        return (Web)Instantiate(web);
    }
    public TreasureHolder GetTreasureHolder()
    {
        return (TreasureHolder)Instantiate(treasureHolder);
    }
}
