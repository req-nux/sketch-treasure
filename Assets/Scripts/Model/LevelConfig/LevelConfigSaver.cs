﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

/// <summary>
/// Fills LevelConfig with data, based on current setup of elements in editor.
/// </summary>
public class LevelConfigSaver : MonoBehaviour {
#if UNITY_EDITOR
    public static void SaveLevel(LevelConfig config, bool overrite = false)
    {
        if(overrite)
        {
            string oldName = config.name;
            int oldId = config.ID;
            string oldDescription = config.devDescription;
            AssetDatabase.DeleteAsset("Assets/Resources/Levels/" + config.name + ".asset");

            LevelConfig newConfig = ScriptableObject.CreateInstance<LevelConfig>();
            newConfig.ID = oldId;
            newConfig.devDescription = oldDescription;
            SaveLevel(newConfig);
            AssetDatabase.CreateAsset(newConfig, "Assets/Resources/Levels/" + oldName + ".asset");
            AssetDatabase.SaveAssets();
            return;
        }

        GameCamera cam = GameObject.FindObjectOfType<GameCamera>();
        config.startCameraPos = cam.transform.position.XY();
        Transform map = GameObject.Find("Map").transform;

        config.startPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        config.startDoor = GameObject.FindGameObjectWithTag("StartDoor").transform.position;
        config.door = GameObject.FindGameObjectWithTag("Door").transform.position;

        config.walls = new List<Vector2>();
        foreach(Wall w in map.Find("Walls").GetComponentsInChildren<Wall>())
            config.walls.Add(w.transform.position);

        config.fields = new List<Vector2>();
        foreach(Field w in map.Find("Fields").GetComponentsInChildren<Field>())
            config.fields.Add(w.transform.position);

        config.webs = new List<WebConfig>();
        foreach(Web w in map.Find("Elements").GetComponentsInChildren<Web>())
            config.webs.Add((WebConfig)w.GetConfig());

        config.rocks = new List<Vector2>();
        foreach(Rock r in map.Find("Elements").GetComponentsInChildren<Rock>())
            config.rocks.Add(r.transform.position);

        config.evilLasers = new List<Vector2>();
        foreach(EvilLaserSource r in map.Find("Elements").GetComponentsInChildren<EvilLaserSource>())
            config.evilLasers.Add(r.transform.position);

        config.bars = new List<BarsConfig>();
        foreach(Bars b in map.Find("Elements").GetComponentsInChildren<Bars>())
            config.bars.Add((BarsConfig)b.GetConfig());

        config.levers = new List<MechanismConfig>();
        foreach(Lever l in map.Find("Elements").GetComponentsInChildren<Lever>())
            config.levers.Add((MechanismConfig)l.GetConfig());

        config.circleLevers = new List<CircleLeverConfig>();
        foreach(CircleLever l in map.Find("Elements").GetComponentsInChildren<CircleLever>())
            config.circleLevers.Add((CircleLeverConfig)l.GetConfig());

        config.buttonFields = new List<MechanismConfig>();
        foreach(ButtonField b in map.Find("Elements").GetComponentsInChildren<ButtonField>())
            config.buttonFields.Add((MechanismConfig)b.GetConfig());

        config.waterFields = new List<Vector2>();
        foreach(WaterField f in map.Find("Elements").GetComponentsInChildren<WaterField>())
            config.waterFields.Add(f.transform.position);

        config.checkpointFields = new List<Vector2>();
        foreach(CheckpointField f in map.Find("Elements").GetComponentsInChildren<CheckpointField>())
            config.checkpointFields.Add(f.transform.position);

        config.chests = new List<Vector2>();
        foreach(Key c in map.Find("Elements").GetComponentsInChildren<Key>())
            config.chests.Add(c.transform.position);

        config.mapSize = FindMapSize(config, map.gameObject);

        config.spiders = new List<SpiderConfig>();
        foreach(Spider s in map.Find("Spiders").GetComponentsInChildren<Spider>())
        {
            SpiderConfig sc = (SpiderConfig)s.GetConfig();
            AddOffsetToSpiderPaths(config, sc);
            config.spiders.Add(sc);
        }

        config.treasureHolder = new List<Vector2>();
        if(GameObject.FindObjectOfType<TreasureHolder>() != null)
            config.treasureHolder.Add(GameObject.FindObjectOfType<TreasureHolder>().transform.position);

    }

    static Vector2 FindMapSize(LevelConfig config, GameObject map)
    {
        float minX = 0, minY = 0, maxX = 0, maxY = 0;

        foreach(Transform t in map.GetComponentsInChildren<Transform>())
        {
            minX = Mathf.Min(minX, t.transform.position.x);
            minY = Mathf.Min(minY, t.transform.position.y);
            maxX = Mathf.Max(maxX, t.transform.position.x);
            maxY = Mathf.Max(maxY, t.transform.position.y);
        }

        return new Vector2(maxX + 1 - minX, maxY + 1 - minY);
    }

    static void AddOffsetToSpiderPaths(LevelConfig lc, SpiderConfig sc)
    {
        Vector2 offset = GameObject.FindObjectOfType<LevelBuilder>().findMapOffset(lc);

        for(int i = 0; i < sc.pathPoints.Count; i++)
            sc.pathPoints[i] += offset;
    }
#endif
}
