﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Reads data from level configuration file in Resources/Levels.
/// Creates instances of objects saved in file and adds them to LevelMap;
/// </summary>
public class LevelBuilder : MonoBehaviour {

    enum ColliderLayer
    {
        Player,
        InteractiveElements,
        Enemies,
        Fields,
    }

    const string levelsPath = "Levels/";

    [SerializeField] Camera gameCamera;
    [SerializeField] LevelMap levelMap;
    [SerializeField] ObjectsPool objectsPool;

    public static Vector2 mapOffset; // position value to compensate, so none of elements has pos X or Y < 0
    public static bool isDuringLevelBuilding = false;
    Field[,] mapArray;

    void Start () {
        objectsPool = GetComponentInChildren<ObjectsPool>();

        if(GameManager.Instance.LoadedLevel.ID != 0)
            BuildLevel(GameManager.Instance.LoadedLevel);
        else
            BuildLevel(LoadLevel(1));
    }

    #if UNITY_EDITOR
    public void RebuildLevel(LevelConfig config)
    {
        BuildLevel(config);
    }
    #endif

    LevelConfig LoadLevel(int levelID)
    {
        return Resources.Load<LevelConfig>(levelsPath + "Level" + levelID.ToString());
    }

    void BuildLevel(LevelConfig lc)
    {
        isDuringLevelBuilding = true;

        levelMap.InitSize(lc.mapSize);
        mapArray = levelMap.Map;
        mapOffset = findMapOffset(lc);

        Vector3 cameraPos = new Vector3(lc.startCameraPos.x, lc.startCameraPos.y, gameCamera.transform.position.z);
        gameCamera.GetComponent<CameraController>().StartPos = cameraPos;
        gameCamera.transform.position = cameraPos;

        SetCameraBounds(lc, mapOffset);

        LevelMap.Player.transform.position = lc.startPos;
        LevelMap.Player.transform.position += new Vector3(0, 0, (int)ColliderLayer.Player);
        LevelMap.Player.MapPosition = lc.startPos + mapOffset;

        Door door = GameObject.FindObjectOfType<Door>();
        door.transform.position = lc.door;
        door.transform.position += new Vector3(0, 0, (int)ColliderLayer.InteractiveElements);
        insertElementToMap(door, lc.door);

        GameObject startDoor = GameObject.FindGameObjectWithTag("StartDoor");
        startDoor.transform.position = lc.startDoor;

        if(lc.treasureHolder.Count > 0)
        {
            TreasureHolder treasureHolder = objectsPool.GetTreasureHolder();
            treasureHolder.transform.position = lc.treasureHolder[0];
            insertElementToMap(treasureHolder, lc.treasureHolder[0]);
        }

        Wall wall;
        Transform wallsContainer = GameObject.Find("Walls").transform;
        foreach(Vector2 wallPos in lc.walls)
        {
            wall = objectsPool.getWall();
            wall.gameObject.SetActive(true);
            wall.transform.position = wallPos;
            wall.transform.SetParent(wallsContainer);
        }

        Field field;
        Transform fieldsContainer = GameObject.Find("Fields").transform;
        foreach(Vector2 fieldPos in lc.fields)
        {
            field = objectsPool.getField();
            field.gameObject.SetActive(true);
            field.transform.position = fieldPos;
            field.transform.position += new Vector3(0, 0, (int)ColliderLayer.Fields);
            field.transform.SetParent(fieldsContainer);
            insertElementToMap(field, fieldPos);
        }
        Transform elementsContainer = GameObject.Find("Elements").transform;

        WaterField waterField;
        foreach(Vector2 waterFieldPos in lc.waterFields)
        {
            waterField = objectsPool.GetWaterField();
            waterField.gameObject.SetActive(true);
            waterField.transform.position = waterFieldPos;
            waterField.transform.position += new Vector3(0, 0, (int)ColliderLayer.Fields);
            waterField.transform.SetParent(elementsContainer);
            insertElementToMap(waterField, waterFieldPos);
        }

        CheckpointField checkpointField;
        foreach(Vector2 checkpointFieldPos in lc.checkpointFields)
        {
            checkpointField = objectsPool.getCheckpointField();
            checkpointField.gameObject.SetActive(true);
            checkpointField.transform.position = checkpointFieldPos;
            checkpointField.transform.position += new Vector3(0, 0, (int)ColliderLayer.Fields);
            checkpointField.transform.SetParent(elementsContainer);
            insertElementToMap(checkpointField, checkpointFieldPos);
        }

        Bars bars;
        foreach(BarsConfig barsConfig in lc.bars)
        {
            bars = objectsPool.getBars();
            bars.transform.position = barsConfig.pos;
            bars.CloseDirection = barsConfig.closeDirection;
            bars.transform.SetParent(elementsContainer);
            insertElementToMap(bars, barsConfig.pos);
            bars.initState(barsConfig.opened);
        }

        ButtonField buttonField;
        foreach(MechanismConfig buttonFieldConfig in lc.buttonFields)
        {
            buttonField = objectsPool.getButtonField();
            buttonField.transform.position = buttonFieldConfig.pos;
            buttonField.transform.position += new Vector3(0, 0, (int)ColliderLayer.Fields);
            buttonField.transform.SetParent(elementsContainer);
            setBarsBasedOnPositions(buttonField.Bars, buttonFieldConfig.bars);
            insertElementToMap(buttonField, buttonFieldConfig.pos);
        }

        Rock rock;
        foreach(Vector2 rockPos in lc.rocks)
        {
            rock = objectsPool.getRock();
            rock.transform.position = rockPos;
            rock.transform.position += new Vector3(0, 0, (int)ColliderLayer.InteractiveElements);
            rock.transform.SetParent(elementsContainer);
            insertElementToMap(rock, rockPos);
        }

        Key chest;
        foreach(Vector2 chestPos in lc.chests)
        {
            chest = objectsPool.getChest();
            chest.transform.position = chestPos;
            chest.transform.position += new Vector3(0, 0, (int)ColliderLayer.InteractiveElements);
            chest.transform.SetParent(elementsContainer);
            insertElementToMap(chest, chestPos);
        }

        EvilLaserSource evilLaser;
        foreach(Vector2 evilLaserPos in lc.evilLasers)
        {
            evilLaser = objectsPool.GetEvilLaser();
            evilLaser.transform.position = evilLaserPos;
            evilLaser.transform.position += new Vector3(0, 0, (int)ColliderLayer.InteractiveElements);
            evilLaser.transform.SetParent(elementsContainer);
            insertElementToMap(evilLaser, evilLaserPos);
        }
        Web web;
        foreach(WebConfig webConfig in lc.webs)
        {
            web = objectsPool.GetWeb();
            web.transform.position = webConfig.pos;
            web.transform.SetParent(elementsContainer);
            web.SetSpriteByConfig(webConfig);
        }
        Lever lever;
        foreach(MechanismConfig leverConfig in lc.levers)
        {
            lever = objectsPool.getLever();
            lever.transform.position = leverConfig.pos;
            lever.transform.position += new Vector3(0, 0, (int)ColliderLayer.InteractiveElements);
            lever.transform.SetParent(elementsContainer);
            setBarsBasedOnPositions(lever.Bars, leverConfig.bars);
            insertElementToMap(lever, leverConfig.pos);
        }

        CircleLever circleLever;
        foreach(CircleLeverConfig circleLeverConfig in lc.circleLevers)
        {
            circleLever = objectsPool.getCircleLever();
            circleLever.transform.position = circleLeverConfig.pos;
            circleLever.transform.position += new Vector3(0, 0, (int)ColliderLayer.InteractiveElements);
            circleLever.transform.SetParent(elementsContainer);
            circleLever.StatesCount = circleLeverConfig.statesCount;
            setBarsBasedOnPositions(circleLever.Bars0, circleLeverConfig.barsState0);
            setBarsBasedOnPositions(circleLever.Bars1, circleLeverConfig.barsState1);
            setBarsBasedOnPositions(circleLever.Bars2, circleLeverConfig.barsState2);
            setBarsBasedOnPositions(circleLever.Bars3, circleLeverConfig.barsState3);
            insertElementToMap(circleLever, circleLeverConfig.pos);
        }

        Transform spidersContainer = GameObject.Find("Spiders").transform;
        Spider spider;
        foreach(SpiderConfig spiderConfig in lc.spiders)
        {
            spider = objectsPool.getSpider();
            spider.transform.position = spiderConfig.pos;
            spider.transform.position += new Vector3(0, 0, (int)ColliderLayer.Enemies);
            spider.transform.SetParent(spidersContainer);
            spider.PathPoints = new List<Vector2>(spiderConfig.pathPoints);
#if UNITY_EDITOR
            if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name.Equals("LevelEditor"))
            {
                for(int i = 0; i < spider.PathPoints.Count; i++)
                    spider.PathPoints[i] -= mapOffset;
            }
#endif
            insertElementToMap(spider, spiderConfig.pos);
            spider.gameObject.SetActive(true);
        }
        isDuringLevelBuilding = false;
        LevelMap.Player.GetComponent<PlayerMovement>().InitPathfinding();
    }


    void insertElementToMap(MapElement elem, Vector2 pos)
    {
        pos = new Vector2(pos.x + mapOffset.x, pos.y + mapOffset.y);
        if(pos.x >= mapArray.GetLength(0) || pos.y >= mapArray.GetLength(1))
            Debug.LogError("LevelBuilder.insertElementToMap: " + elem.name + " position out of map! " + pos
                + " where map size is " + string.Format("[{0}][{1}]", mapArray.GetLength(0), mapArray.GetLength(1)));

        if(elem is Field)
            mapArray[(int)pos.x, (int)pos.y] = (Field)elem;
        else if(elem.OccupiesField)
        {
            mapArray[(int)pos.x, (int)pos.y].ElemOnField = elem;
        }
        elem.MapPosition = pos;

    }

    /// <summary>
    /// Finds offset for all elements in map;
    /// Required, so elements wont be added with negative index values to map array
    /// </summary>
    /// <returns>If lowest element on map(in editor) has position (17,-9)
    /// return value is (0,9)</returns>
    public Vector2 findMapOffset(LevelConfig lc)
    {
        float maxOffsetX = 0;
        float maxOffsetY = 0;
        
        foreach(Vector2 wallPos in lc.walls)
        {
            if(wallPos.x < maxOffsetX)
                maxOffsetX = wallPos.x;
            if(wallPos.y < maxOffsetY)
                maxOffsetY = wallPos.y;
        }
        if(lc.door.x < maxOffsetX)
            maxOffsetX = lc.door.x;
        if(lc.door.y < maxOffsetY)
            maxOffsetY = lc.door.y;
        return new Vector2(Mathf.Abs(maxOffsetX), Mathf.Abs(maxOffsetY));
    }

    void setBarsBasedOnPositions(List<Bars> bars, List<Vector2> barsPos)
    {
        foreach(Vector2 barPos in barsPos)
        {
            if(levelMap.GetElement<Bars>(barPos + mapOffset) != null)
                bars.Add(levelMap.GetElement<Bars>(barPos + mapOffset));
            else
            {
                // if bars are opened at level start find bars on given position
                Bars[] b = GameObject.FindObjectsOfType<Bars>();
                bars.Add(System.Array.Find<Bars>(b, x => x.MapPosition == barPos + mapOffset));
            }
        }
    }

    void SetCameraBounds(LevelConfig lc, Vector2 minOffset)
    {
        Vector2 maxOffset = GetMaxWallPos(lc);
        gameCamera.GetComponent<CameraController>().MaxLevelOffset = maxOffset;
        gameCamera.GetComponent<CameraController>().MinLevelOffset = minOffset;
    }

    Vector2 GetMaxWallPos(LevelConfig lc)
    {
        float maxOffsetX = Int32.MinValue;
        float maxOffsetY = Int32.MinValue;

        foreach(Vector2 wallPos in lc.walls)
        {
            if(wallPos.x > maxOffsetX)
                maxOffsetX = wallPos.x;
            if(wallPos.y > maxOffsetY)
                maxOffsetY = wallPos.y;
        }
        return new Vector2(maxOffsetX, maxOffsetY);
    }
}

