﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpiderConfig : ElementConfig
{
    public Vector2 pos;
    public List<Vector2> pathPoints; //last point of pathPoint should be equal to pos

    public SpiderConfig(Vector2 pos, List<Vector2> pathPoints)
    {
        this.pos = pos;
        this.pathPoints = pathPoints;
    }
}
