﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BarsConfig : ElementConfig {

    public Vector2 pos;
    public bool opened;
    public CloseDirection closeDirection;

    public BarsConfig(Vector2 pos, bool opened, CloseDirection closeDirection)
    {
        this.pos = pos;
        this.opened = opened;
        this.closeDirection = closeDirection;
    }
}
