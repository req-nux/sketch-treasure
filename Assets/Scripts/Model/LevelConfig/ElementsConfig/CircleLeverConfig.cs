﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CircleLeverConfig : ElementConfig {

    public Vector2 pos;
    public int statesCount;
    public List<Vector2> barsState0;
    public List<Vector2> barsState1;
    public List<Vector2> barsState2;
    public List<Vector2> barsState3;

    public CircleLeverConfig (
        Vector2 pos,
        int statesCount, 
        List<Vector2> barsState0, 
        List<Vector2> barsState1, 
        List<Vector2> barsState2, 
        List<Vector2> barsState3)
    {
        this.pos = pos;
        this.statesCount = statesCount;
        this.barsState0 = barsState0;
        this.barsState1 = barsState1;
        this.barsState2 = barsState2;
        this.barsState3 = barsState3;
    }
}
