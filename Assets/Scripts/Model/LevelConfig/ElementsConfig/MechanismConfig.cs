﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MechanismConfig : ElementConfig {

    public Vector2 pos;
    public List<Vector2> bars;

    public MechanismConfig(Vector2 pos, List<Vector2> bars)
    {
        this.pos = pos;
        this.bars = bars;
    }
}
