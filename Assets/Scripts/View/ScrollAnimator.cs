﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollAnimator : MonoBehaviour {

    [SerializeField] RectTransform scrollRight;
    [SerializeField] RectTransform scrollLeft;
    [SerializeField] float openedAnchoredPosX = 500;
    [SerializeField] float closedAnchoredPosX = 46;
    [SerializeField] AnimationCurve dumping;
    [SerializeField] float animTime = 1.5f;
    [SerializeField] float foldPercentageToPlaySound = 0.4f;
    [SerializeField] CommonEngine.ScriptableObjects.AudioObject scrollFoldSound;

    bool isAnimating;

    void Awake()
    {
        if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name.Equals("Gameplay"))
        {
            OpenScroll(() => 
                {
                    GetComponent<ImageFade>().TargetImage.GetComponent<Button>().interactable = true;
                    GetComponent<ImageFade>().Fade();
                });
        }
        else
        {
            OpenScroll(null);
        }
    }

    public void OpenScroll(Action onScrollOpen)
    {
        SoundManager.Instance.TurnMusicVolume(4f,SoundManager.Instance.DefaultMusicVolume);
        StopAllCoroutines();
        scrollRight.anchoredPosition = new Vector2(closedAnchoredPosX, scrollRight.anchoredPosition.y);
        scrollLeft.anchoredPosition = new Vector2(-closedAnchoredPosX, scrollRight.anchoredPosition.y);

        StartCoroutine(ScrollAnimationCoroutine(true, onScrollOpen));
    }
    public void CloseScroll(Action onScrollClose)
    {
        StopAllCoroutines();
        scrollRight.anchoredPosition = new Vector2(openedAnchoredPosX, scrollRight.anchoredPosition.y);
        scrollLeft.anchoredPosition = new Vector2(-openedAnchoredPosX, scrollRight.anchoredPosition.y);
        StartCoroutine(ScrollAnimationCoroutine(false, onScrollClose));
    }

    IEnumerator ScrollAnimationCoroutine(bool open, Action onAnimationFinished)
    {
        isAnimating = true;

        float distance = openedAnchoredPosX - closedAnchoredPosX;
        float step = distance / animTime;
        float destination = open ? openedAnchoredPosX : closedAnchoredPosX;
        float missingDistance = distance;

        bool soundPlayed = false;
        
        while(missingDistance > step*0.3*Time.deltaTime)
        {
            float stepDumping = dumping.Evaluate(missingDistance / distance);

            if(open)
            {
                scrollRight.anchoredPosition += new Vector2(step * stepDumping * Time.deltaTime, 0);
                scrollLeft.anchoredPosition += new Vector2(-step * stepDumping * Time.deltaTime, 0);
            }
            else
            {
                scrollRight.anchoredPosition += new Vector2(-step * stepDumping * Time.deltaTime, 0);
                scrollLeft.anchoredPosition += new Vector2(step * stepDumping * Time.deltaTime, 0);
            }
            missingDistance = Mathf.Abs(scrollRight.anchoredPosition.x - destination);

            if(!soundPlayed && missingDistance < distance * foldPercentageToPlaySound)
            {
                SoundManager.Instance.PlayFX(scrollFoldSound);
                soundPlayed = true;
            }
            yield return null;
        }
        scrollRight.anchoredPosition = new Vector2(open ? openedAnchoredPosX : closedAnchoredPosX, scrollRight.anchoredPosition.y);
        scrollLeft.anchoredPosition = new Vector2(open ? -openedAnchoredPosX : -closedAnchoredPosX, scrollLeft.anchoredPosition.y);

        if(onAnimationFinished != null)
            onAnimationFinished();

        isAnimating = false;
    }
    public bool IsAnimating
    {
        get { return isAnimating; }
    }
}
