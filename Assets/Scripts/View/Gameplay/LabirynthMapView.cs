﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LabirynthMapView : MonoBehaviour {

    [SerializeField] float segmentFadeTime = 2f;
    [SerializeField] float segmentFadeDelay = 1f;
    [SerializeField] ScrollAnimator scrollAnimator;

    Button[] segmentButtons;

    private void Start()
    {
        segmentButtons = GetComponentsInChildren<Button>();
    }

    public void DisableButtons()
    {
        foreach(Button button in segmentButtons)
        {
            button.interactable = false;
        }
    }
    public void OnStateLoad()
    {
        
        if(GameManager.Instance.SceneManager.SceneChangeMode == SceneChangeMode.OnLevelComplete)
        {
            MapSegment[] unlockedSegments = FindLastUnlockedSegments();
            foreach(var s in unlockedSegments)
                s.GetComponent<SegmentButton>().SetUnlocked(false);
            StartCoroutine(FadeSegmentsAfterScrollOpened(unlockedSegments));
        }
    }

    IEnumerator FadeSegmentsAfterScrollOpened(MapSegment[] unlockedSegments)
    {
        yield return new WaitUntil(() => !scrollAnimator.IsAnimating);
        foreach(var s in unlockedSegments)
                s.GetComponent<SegmentButton>().FadeToUnlocked(segmentFadeTime, segmentFadeDelay);
    }

    MapSegment[] FindLastUnlockedSegments()
    {
        MapSegment[] ms = GetComponent<LabirynthMap>().MapSegments;
        MapSegment completedSegment
            = System.Array.Find<MapSegment>(ms, x => x.Level.ID == GameManager.Instance.LoadedLevel.ID);

        return System.Array.FindAll<MapSegment>(ms, x => completedSegment.UnlockedSegmentsIDs.Contains(x.Level.ID));
    }


}
