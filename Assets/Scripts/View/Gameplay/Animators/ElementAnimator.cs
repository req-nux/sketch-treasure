﻿using System.Collections;
using System.Linq;
using UnityEngine;

/// <summary>
/// Controlls animation of an element based on AnimState
/// </summary>
public class ElementAnimator : MonoBehaviour {

    protected Animator anim;
    protected SpriteMask mask;
    

    protected virtual void Awake () {
        Init();
    }

    public void Init()
    {
        anim = GetComponent<Animator>();
        mask = GetComponentInChildren<SpriteMask>();
    }

    public void HandleMovementDirection(Direction dir)
    {
        switch(dir)
        {
            case Direction.Up: SetState(AnimState.moveUp);break;
            case Direction.Down: SetState(AnimState.moveDown);break;
            case Direction.Left: SetState(AnimState.moveLeft);break;
            case Direction.Right: SetState(AnimState.moveRight);break;
        }
    }
    public void HandleRockPushDirection(Direction dir)
    {
        switch(dir)
        {
            case Direction.Up: SetState(AnimState.pushRockUp);break;
            case Direction.Down: SetState(AnimState.pushRockDown);break;
            case Direction.Left: SetState(AnimState.pushRockLeft);break;
            case Direction.Right: SetState(AnimState.pushRockRight);break;
        }
    }
    public void HandleLeverUse(Direction dir, Lever lever, bool changeBarsState)
    {
        ElementAnimator leverAnim = lever.GetComponent<ElementAnimator>();
        AnimState state = AnimState.leverFromRight_Right;
        float leverAnimDelay = 0;

        switch(dir)
        {
            case Direction.Up: state = lever.Active ? AnimState.leverFromTop_Right : AnimState.leverFromTop_Left; break;
            case Direction.Down: state = lever.Active ? AnimState.leverFromDown_Left : AnimState.leverFromDown_Right; break;
            case Direction.Left: state = lever.Active ? AnimState.leverFromRight_Right : AnimState.leverFromRight_Left; break;
            case Direction.Right: state = lever.Active ? AnimState.leverFromLeft_Left:AnimState.leverFromLeft_Right;break;
        }
        switch(state)
        {
            case AnimState.leverFromRight_Right: leverAnimDelay = 1.1f; Debug.Log(state.ToString());break;
            case AnimState.leverFromRight_Left: leverAnimDelay = 0.45f; Debug.Log(state.ToString());break;
            case AnimState.leverFromLeft_Right: leverAnimDelay = 1.1f;Debug.Log(state.ToString()); break;
            case AnimState.leverFromLeft_Left: leverAnimDelay = 0.45f; Debug.Log(state.ToString());break;
            default: leverAnimDelay = 1.45f; break;
        }
        if(changeBarsState)
            StartCoroutine(DelayAction(leverAnimDelay, () => { leverAnim.HandleLeverStateChange(!lever.Active); lever.ChangeBarsState(); }));
        else
            StartCoroutine(DelayAction(leverAnimDelay, () => leverAnim.HandleLeverStateChange(!lever.Active)));
        SetState(state);
    }
    public void HandleLeverUseFailedAttempt(Direction dir)
    {
        switch(dir)
        {
            case Direction.Up: SetState(AnimState.pushRockUp);break;
            case Direction.Down: SetState(AnimState.pushRockDown);break;
            case Direction.Left: SetState(AnimState.pushRockLeft);break;
            case Direction.Right: SetState(AnimState.pushRockRight);break;
        }
    }
    public void HandleBarsStateChange(bool destBarsState, CloseDirection closeDir)
    {
        AnimState state = AnimState.barsOpenUp;
        switch(closeDir)
        {
            case CloseDirection.Up: state = destBarsState ? AnimState.barsOpenUp : AnimState.barsCloseUp; break;
            case CloseDirection.Right: state = destBarsState ? AnimState.barsOpenRight : AnimState.barsCloseRight; break;
        }
        float delay = GetAnimDelayByAnimState(state);
        StartCoroutine(DelayAction(delay, () => SetState(state)));
        
    }
    public void HandleLeverStateChange(bool destLeverState)
    {
        AnimState state = destLeverState ? AnimState.leverLeft : AnimState.leverRight;
        SetState(state);
    }
    public void HandleDoorOpen()
    {
        anim.Play("DoorOpen");
    }
    public void HandleSpiderMovementDirection(Direction dir)
    {
        switch(dir)
        {
            case Direction.Up: transform.eulerAngles = new Vector3(0,0,180);break;
            case Direction.Down: transform.eulerAngles = Vector3.zero;break;
            case Direction.Left: transform.eulerAngles = new Vector3(0,0,-90);break;
            case Direction.Right: transform.eulerAngles = new Vector3(0,0,90);break;
        }
    }
    public void HandleButtonFieldStateChange(bool pressed)
    {
        SetState(pressed ? AnimState.buttonFieldPressed : AnimState.buttonFieldReleased);
    }
    public void HandleCheckpointFieldStateChange(bool active)
    {
        SetState(active ? AnimState.checkpointFieldActivation : AnimState.checkpointFieldDeactivation);
    }

    public void SetState(AnimState state)
    {
        if(state == AnimState.idle)
            state.ToString();
        anim.SetInteger("state", (int)state);
    }
    public void PlayStateImmediate(string stateName)
    {
        SetState(AnimState.UNDEFINED);
        anim.Play(stateName, -1, 0.99f);
    }
    public void Stop()
    {
        anim.speed = 0;
    }
    public AnimState GetCurrentState()
    {
        return (AnimState)anim.GetInteger("state");
    }
    public float GetClipLenghtByName(string clipName)
    {
        return anim.runtimeAnimatorController.animationClips.First(x => x.name.Equals(clipName)).length;
    }
    public float GetCurrentClipLenght()
    {
        return anim.GetCurrentAnimatorStateInfo(0).length;
    }

    public float GetAnimDelayByAnimState(AnimState state)
    {
        switch(state)
        {
            case AnimState.leverRight: return 0.26f;
            case AnimState.leverLeft: return 1.05f;
            case AnimState.barsCloseUp: return 0.16f;
            case AnimState.barsOpenUp: return 0.16f;

            default: return 0;                
        }
    }

    IEnumerator DelayAction(float delay, System.Action action)
    {
        yield return new WaitForSeconds(delay);
        action();
    }
    public SpriteMask Mask
    {
        get { return mask;  }
    }
}
