﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMaskUpdater : MonoBehaviour {

    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] SpriteMask spriteMask;
    [SerializeField] List<SpriteMaskPair> spriteMaskPairList;
    Dictionary<Sprite, Sprite> spriteMaskDictionary;

    Sprite lastSprite;

    void Awake()
    {
        CreateDictionaryFromList();
        UpdadeMask();
    }


    void Update () {
        if(lastSprite != spriteRenderer.sprite)
        {
            UpdadeMask();
        }
	}

    void UpdadeMask()
    {
        spriteMask.sprite = spriteMaskDictionary.ContainsKey(spriteRenderer.sprite) 
                            ? spriteMaskDictionary[spriteRenderer.sprite] : null;

        lastSprite = spriteRenderer.sprite;
        transform.rotation = spriteRenderer.flipX ? Quaternion.Euler(0, 180, 0) : Quaternion.Euler(0, 0, 0);
    }

    void CreateDictionaryFromList()
    {
        spriteMaskDictionary = new Dictionary<Sprite, Sprite>();
        foreach(SpriteMaskPair pair in spriteMaskPairList)
        {
            spriteMaskDictionary.Add(pair.sprite, pair.mask);
        }
    }
}

[System.Serializable]
public class SpriteMaskPair
{
    public Sprite sprite;
    public Sprite mask;
}
