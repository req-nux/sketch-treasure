﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleLeverAnimator : ElementAnimator {

    [SerializeField] float fullRotationTime = 4f;
    Transform spriteTransform;

    protected override void Awake()
    {
        spriteTransform = transform.Find("sprite");
    }

    public void handleCircleLeverStateChange(int stateCount)
    {
        float rotationAngle = 360f / stateCount;
        StartCoroutine(rotationAnimation(rotationAngle));
    }

    public void setStateImmediate(int state, int stateCount)
    {
        float rotation = state * 360f / stateCount;
        spriteTransform.eulerAngles = new Vector3(0, 0, rotation);
    }

    IEnumerator rotationAnimation(float rotationAngle)
    {
        float destZRot = spriteTransform.eulerAngles.z - rotationAngle;
        float rotationStep = rotationAngle / (fullRotationTime * rotationAngle/360f);

        float rotationLeft = rotationAngle;
        while(rotationLeft > rotationStep * Time.deltaTime)
        {
            spriteTransform.Rotate(0,0, -rotationStep * Time.deltaTime);
            rotationLeft -= rotationStep * Time.deltaTime;
            yield return null;
        }
        spriteTransform.eulerAngles = new Vector3(0, 0, destZRot);
    }


}
