﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementHighlighting : MonoBehaviour {

    ObjectsPool objectPool;
    GameObject[] activeHighlights;
    bool elemsHighlighted = false;

    void Awake () {
        objectPool = GameObject.FindObjectOfType<ObjectsPool>();
    }

    public void setHighlights(bool state, List<Bars> bars)
    {
        if(state && !elemsHighlighted)
        {
            if(activeHighlights == null || bars.Count > activeHighlights.Length)
                activeHighlights = new GameObject[bars.Count];

            for(int i = 0; i < bars.Count; i++)
            {
                var hl = objectPool.getHighlight();
                activeHighlights[i] = hl;
                hl.transform.position = bars[i].transform.position;
                hl.SetActive(true);
            }
            elemsHighlighted = true;
        }
        else if(!state && elemsHighlighted)
        {
            for(int i = 0; i < activeHighlights.Length; i++)
                activeHighlights[i].SetActive(false);
            elemsHighlighted = false;
        }
    }

}
