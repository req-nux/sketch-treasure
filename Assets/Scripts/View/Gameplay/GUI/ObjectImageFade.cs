﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectImageFade : MonoBehaviour {

    float defaultFadeTime = 2.5f;
    float fadeDelayTime = 1.5f;

    public void fadeToSolid(GameObject go)
    {
        List<Image> sprites = new List<Image>();
        sprites.AddRange(go.transform.GetComponentsInChildren<Image>());
        if(go.GetComponent<Image>() != null)
            sprites.Add(go.GetComponent<Image>());
        //setToTransparent(sprites);
        StartCoroutine(fade(sprites,true, defaultFadeTime, fadeDelayTime));
    }

    public void fadeToTransparent(GameObject go)
    {
        List<Image> sprites = new List<Image>();
        sprites.AddRange(go.transform.GetComponentsInChildren<Image>());
        if(go.GetComponent<Image>() != null)
            sprites.Add(go.GetComponent<Image>());
        StartCoroutine(fade(sprites, false, defaultFadeTime, fadeDelayTime));
    }

    IEnumerator fade(List<Image> sr, bool fadeToBlack, float time, float delay)
    {
        yield return new WaitForSeconds(delay);
        bool finished = false;
        int steps = 30;
        float alphaStepSign = fadeToBlack ? 1f : -1f;
        float alphaStepValue = alphaStepSign / steps;
        float waitTimeValue = time / steps;

        while(!finished)
        {
            finished = true;
            foreach(var s in sr)
            {
                s.color = new Color(s.color.r, s.color.g, s.color.b, s.color.a + alphaStepValue);
                if(alphaStepSign == 1)
                {
                    if(s.color.a != 1)
                        finished = false;
                }
                else if(s.color.a != 0)
                        finished = false;
            }
            yield return new WaitForSeconds(waitTimeValue);
        }
    }

    void setToTransparent(List<Image> images)
    {
        foreach(var i in images)
            i.color = new Color(i.color.r, i.color.g, i.color.b, 0f);
    }

    public float TotalFadeTime
    {
        get { return defaultFadeTime + fadeDelayTime; }
    }

}
