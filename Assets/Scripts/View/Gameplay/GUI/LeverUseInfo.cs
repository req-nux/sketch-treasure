﻿using UnityEngine;

public class LeverUseInfo : MonoBehaviour {

	[SerializeField] PopupInfoPanel leverUsePanelPrefab;
    [SerializeField] RectTransform gameMenuCanvas;
    [SerializeField] LevelConfig showOnLevel;

    void Awake()
    {
        if(GameManager.Instance.LoadedLevel.ID == showOnLevel.ID)
        {
            PopupInfoPanel leverUsePanel = Instantiate(leverUsePanelPrefab, gameMenuCanvas, false) as PopupInfoPanel;
            leverUsePanel.transform.SetAsFirstSibling();
            leverUsePanel.Show();
        }
    }

}
