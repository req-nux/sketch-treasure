﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionMark : MonoBehaviour {

    [SerializeField] float markFillTime;
    [SerializeField] CommonEngine.ScriptableObjects.AudioObject drawMarkSound;
    Image markImage;

    void Awake()
    {
        markImage = GetComponent<Image>();
    }

    public void SetMark(Vector3 pos)
    {
        StopAllCoroutines();
        transform.position = pos;
        markImage.fillAmount = 0;
        markImage.enabled = true;
        transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
        SoundManager.Instance.PlayFX(drawMarkSound);
        StartCoroutine(FillMark());
    }
    public void Hide()
    {
        StopAllCoroutines();
        markImage.enabled = false;
    }

    IEnumerator FillMark()
    {
        float fillStep = 1 / markFillTime;

        while(markImage.fillAmount < 1)
        {
            markImage.fillAmount += fillStep * Time.deltaTime;
            yield return null;
        }
    }
}
