﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CommonEngine.ScriptableObjects;

public class ImageFade : MonoBehaviour {

    [SerializeField] Image targetImage;
    [SerializeField] ColorPreset finalColor;
    [SerializeField] float fadeTime;

    //Called when component is added in the inspector
    void Reset()
    {
        if(targetImage == null)
            targetImage = gameObject.GetComponent<Image>();
        if(fadeTime == 0)
            fadeTime = 2f;
    }

    public void Fade()
    {
        Fade(null);
    }
    public void Fade(System.Action onFadeFinished)
    {
        StartCoroutine(FadeImage(onFadeFinished));
    }

    IEnumerator FadeImage(System.Action onFadeFinished)
    {
        Color colorDiff = finalColor - targetImage.color;
        Color colorStep = colorDiff / fadeTime;

        float maxDiff = Mathf.Abs(colorDiff.maxColorComponent) > Mathf.Abs(colorDiff.a) 
                        ? Mathf.Abs(colorDiff.maxColorComponent) : Mathf.Abs(colorDiff.a);
        float maxStep = Mathf.Abs(colorStep.maxColorComponent) > Mathf.Abs(colorStep.a) 
                        ? Mathf.Abs(colorStep.maxColorComponent) : Mathf.Abs(colorStep.a);
        int steps = Mathf.FloorToInt(maxDiff / (maxStep * Time.deltaTime));

        for(int i = 0; i < steps; i++)
        {
            targetImage.color += colorStep * Time.deltaTime;
            yield return null;
        }
        targetImage.color = finalColor;

        if(onFadeFinished != null)
            onFadeFinished();
    }

    public Image TargetImage
    {
        get
        {
            return targetImage;
        }

        set
        {
            targetImage = value;
        }
    }
}
