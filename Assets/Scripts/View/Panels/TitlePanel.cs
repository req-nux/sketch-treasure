﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TitlePanel : MonoBehaviour, UnityEngine.EventSystems.IPointerClickHandler
{
    static bool clicked = false;
    [SerializeField] ScrollAnimator scrollAnimator;

    void Awake()
    {
        if(clicked)
            gameObject.SetActive(false);
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        if(clicked)
            return;

        clicked = true;
        scrollAnimator.CloseScroll(() => 
            {
                scrollAnimator.OpenScroll(null);
                gameObject.SetActive(false);
            });
    }
}
