﻿using UnityEngine;
using UnityEngine.UI;

public class Panel : MonoBehaviour {

    [SerializeField] float fadeInTime;
    [SerializeField] float fadeOutTime;

    public void show()
    {
        gameObject.SetActive(true);
    }
    public void hide()
    {
        gameObject.SetActive(false);
    }

    protected void fadeIn()
    {

    }

    protected void fadeOut()
    {

    }
}
