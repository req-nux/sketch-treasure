﻿using UnityEngine;
using UnityEngine.UI;

public class TeleportationToggle : MonoBehaviour {

    public void onValueChange()
    {
        GameObject.FindObjectOfType<PlayerController>().teleportation = !GetComponent<Toggle>().isOn;
    }

}
