﻿using CommonEngine.ScriptableObjects;
using UnityEngine;
using UnityEngine.EventSystems;

public class PopupInfoPanel : MonoBehaviour, IPointerClickHandler {

    [SerializeField] float minDisplayTime = 1f;
    [SerializeField] AudioObject openCloseSound;
    float showTime;

    public void Show()
    {
        gameObject.SetActive(true);
        showTime = Time.time;
        SoundManager.Instance.PlayFX(openCloseSound);
    }

    void Hide()
    {
        gameObject.SetActive(false);
        SoundManager.Instance.PlayFX(openCloseSound);
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        if(Time.time > showTime + minDisplayTime)
        {
            Hide();
        }
    }
}
