﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RateAppPanel : MonoBehaviour {

    [SerializeField] string rateAppURL;
    [SerializeField] Button rateButton;
    [SerializeField] Button cancelButton;

    void Awake () {
        rateButton.onClick.AddListener(OnRate);
        cancelButton.onClick.AddListener(OnCancel);
	}
	
	void OnRate () {
        rateButton.interactable = false;
        cancelButton.interactable = false;

        Application.OpenURL(rateAppURL);
        QuitToMap();

    }

    void OnCancel()
    {
        rateButton.interactable = false;
        cancelButton.interactable = false;
        QuitToMap();
    }
    void QuitToMap()
    {
        ScrollAnimator scrollAnimator = GameObject.FindObjectOfType<ScrollAnimator>();
        GameManager.Instance.SceneManager.LoadScene("MainMenu", SceneChangeMode.OnLevelComplete);
    }
}
