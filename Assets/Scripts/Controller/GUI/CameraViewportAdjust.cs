﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraViewportAdjust : MonoBehaviour {

    float aspect = 1.706667f;

    void Awake()
    {
        adjustViewport();
    }

    public void adjustViewport()
    {
        Camera.main.projectionMatrix = Matrix4x4.Ortho(
           -Camera.main.orthographicSize * aspect, Camera.main.orthographicSize * aspect,
           -Camera.main.orthographicSize, Camera.main.orthographicSize,
           Camera.main.nearClipPlane, Camera.main.farClipPlane);
    }
}
