﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestartLevelButton : ButtonBase {

    [SerializeField] Button leaveChamberButton;
    public override void onClick()
    {
        leaveChamberButton.interactable = false;
        GetComponent<UnityEngine.UI.Button>().interactable = false;
        Time.timeScale = 1;
        GameManager.Instance.SceneManager.RestartCurrentScene();
    }
}
