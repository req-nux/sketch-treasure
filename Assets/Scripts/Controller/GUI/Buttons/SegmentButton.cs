﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEngine.ScriptableObjects;
using UnityEngine.UI;

public class SegmentButton : ButtonBase {

    [SerializeField] Button enterChamberButton;
    [SerializeField] PopupInfoPanel keyMissingPanel;
    [SerializeField] ColorPreset hiddenColor;
    [SerializeField] ColorPreset unlockedColor;
    [SerializeField] ColorPreset selectedColor;
    [SerializeField] LevelSelectionMark mark;

    Image segmentFrame;
    Image[] segmentLines;
    bool selected = false;

    public void Init()
    {
        segmentFrame = GetComponent<Image>();
        segmentLines = new Image[transform.childCount];
        for(int i = 0; i < transform.childCount; i++)
        {
            segmentLines[i] = transform.GetChild(i).GetComponent<Image>();
        }
    }

    public override void onClick()
    {
        KeyRequirement keyRequirement = GetComponent<KeyRequirement>();
        if(keyRequirement != null && !keyRequirement.PlayerHasKey())
        {
            if(GameManager.Instance.SelectedSegment != null)
                GameManager.Instance.SelectedSegment.GetComponent<SegmentButton>().SetSelected(false);
            keyMissingPanel.Show();
        }
        else
        {
            SetSelected(!selected);
        }
    }

    public void SetUnlocked(bool unlocked)
    {
        GetComponent<Button>().interactable = unlocked;
        if(unlocked)
        {
            segmentFrame.color = unlockedColor;
            foreach(var line in segmentLines)
                line.color = unlockedColor;
        }
        else
        {
            segmentFrame.color = hiddenColor;
            foreach(var line in segmentLines)
            {
                line.color = new Color(line.color.r, line.color.g, line.color.b, 0f);
            }
        }
    }

    public void SetSelected(bool s)
    {
        selected = s;

        if(s && GameManager.Instance.SelectedSegment != null &&
            GameManager.Instance.SelectedSegment != GetComponent<MapSegment>())
        {
            GameManager.Instance.SelectedSegment.GetComponent<SegmentButton>().SetSelected(false);
        }

        GameManager.Instance.SelectedSegment = s ? GetComponent<MapSegment>() : null;

        if(s)
        {
            mark.SetMark(transform.position);
        }
        else
        {
            mark.Hide();
        }
        enterChamberButton.interactable = s;
    }

    public void FadeToUnlocked(float fadeTime, float delay)
    {
        SetUnlocked(false);
        foreach(Image line in segmentLines)
        {
            line.GetComponent<ImageFade>().Fade();
        }
        segmentFrame.GetComponent<ImageFade>().Fade(() => SetUnlocked(true));
    }
    
}
