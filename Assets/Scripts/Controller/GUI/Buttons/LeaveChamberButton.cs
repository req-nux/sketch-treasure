﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaveChamberButton : ButtonBase
{
    [SerializeField] GameMenu gameMenu;
    [SerializeField] Button restartLevelButton;

    public override void onClick()
    {
        gameMenu.HideMenuButton();
        restartLevelButton.interactable = false;
        GetComponent<UnityEngine.UI.Button>().interactable = false;
        Time.timeScale = 1;
        GameManager.Instance.SceneManager.LoadScene("MainMenu", SceneChangeMode.LeaveChamber);
    }
}
