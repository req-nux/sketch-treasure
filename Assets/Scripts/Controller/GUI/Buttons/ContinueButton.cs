﻿using UnityEngine;

public class ContinueButton : MonoBehaviour {

    [SerializeField] GameMenu menu;

	void Awake () {
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => menu.OpenCloseMenu());
	}
}
