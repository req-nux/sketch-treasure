﻿using UnityEngine;
using UnityEngine.UI;

public class MusicToggleButton : MonoBehaviour
{
    void Awake()
    {
        GetComponent<Toggle>().isOn = !SoundManager.Instance.MusicEnabled;
        GetComponent<Toggle>().onValueChanged.AddListener(OnClick);
    }

    void OnClick(bool isOn)
    {
        SoundManager.Instance.MusicEnabled = !isOn;
    }
}
