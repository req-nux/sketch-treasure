﻿using UnityEngine;

public class SwitchPanelsButton : ButtonBase {

    [SerializeField] Panel turnOffPanel = null;
    [SerializeField] Panel turnOnPanel = null;

    void Start()
    {

    }

    public override void onClick()
    {
        base.onClick();
        turnOffPanel.hide();
        turnOnPanel.show();
    }
}
