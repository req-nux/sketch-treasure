﻿using UnityEngine;
using UnityEngine.UI;

public class SoundToggleButton : MonoBehaviour {

	void Awake () {
        GetComponent<Toggle>().isOn = !SoundManager.Instance.FxEnabled;
        GetComponent<Toggle>().onValueChanged.AddListener(OnClick);
	}
	
	void OnClick (bool isOn) {
        SoundManager.Instance.FxEnabled = !isOn;
	}
}
