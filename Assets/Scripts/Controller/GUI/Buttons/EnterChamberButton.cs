﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterChamberButton : ButtonBase {

    [SerializeField] LabirynthMapView mapView;
    [SerializeField] ScrollAnimator scrollAnimator;

    public override void onClick()
    {
        base.onClick();
        mapView.DisableButtons();
        GetComponent<UnityEngine.UI.Button>().interactable = false;
        scrollAnimator.CloseScroll(LoadLevel);
    }

    void LoadLevel()
    {
        if(GameManager.Instance.SelectedSegment != null)
            UnityEngine.SceneManagement.SceneManager.LoadScene("Gameplay");
    }

}
