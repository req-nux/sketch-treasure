﻿using UnityEngine;

public abstract class ButtonBase : MonoBehaviour {

    public virtual void onClick() { }
}
