﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Handles touches on MapElements.
/// Calls player actions based on touched MapElement.
/// </summary>
public class PlayerController : MonoBehaviour
{
    public bool teleportation = false;

    float touchTime;
    IHighlighter lastHighlighter;

    Collider2D touchedElem;
    Vector3? touchBeganPoint;
    float maxTouchDiff;

    Camera gameCamera;

    void Awake()
    {
        gameCamera = Camera.main;
    }

    void Update()
    {
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.Mouse0) && !EventSystem.current.IsPointerOverGameObject())
        {
            RaycastHit2D hit = Physics2D.Raycast(gameCamera.ScreenToWorldPoint(Input.mousePosition), Vector3.forward);

            if(hit.collider != null && hit.collider.GetComponent<MapElement>() != null)
            {
                HandleTouchedElement(hit.collider.GetComponent<MapElement>());
            }
        }
#endif
        if(Input.touchCount > 0 && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
        {
            var touch = Input.GetTouch(0);

            RaycastHit2D hit = Physics2D.Raycast(gameCamera.ScreenToWorldPoint(touch.position), Vector3.forward);
            if(hit.collider == null)
                return;

            float cameraWidth = 2 * gameCamera.orthographicSize * gameCamera.aspect;
            if(touchBeganPoint != null)
            {
                Vector3 currentPoint = touch.position;
                float magnitude = (currentPoint - touchBeganPoint).Value.magnitude;
                maxTouchDiff = magnitude > maxTouchDiff ? magnitude : maxTouchDiff;
            }

            if(touch.phase == TouchPhase.Began)
            {
                touchTime = Time.time;
                touchedElem = hit.collider;
                touchBeganPoint = touch.position;
            }
            else if(touch.phase == TouchPhase.Stationary && Time.time > touchTime + 0.5f
                && hit.collider.GetComponent<IHighlighter>() != null)
            {
                if(lastHighlighter != null)
                    lastHighlighter.SetHighlight(false);
                lastHighlighter = hit.collider.GetComponent<IHighlighter>();
                lastHighlighter.SetHighlight(true);
                touchedElem = null;
            }
            else if(touch.phase == TouchPhase.Ended)
            {
                if(hit.collider == touchedElem && maxTouchDiff < Screen.width/cameraWidth/3 && hit.collider.GetComponent<MapElement>() != null)
                    HandleTouchedElement(hit.collider.GetComponent<MapElement>());

                touchedElem = null;
                touchBeganPoint = null;
                maxTouchDiff = 0;
            }
        }

        if(Input.touchCount == 0 && lastHighlighter != null)
        {
            lastHighlighter.SetHighlight(false);
            lastHighlighter = null;
            touchedElem = null;
        }
    }

    void HandleTouchedElement(MapElement elem)
    {
        if(teleportation && elem is Field)
        {
            GameObject.FindObjectOfType<LevelMap>().MoveElement(LevelMap.Player, elem.MapPosition);
            LevelMap.Player.transform.position = elem.transform.position;
        }
        else
        {
            LevelMap.Player.InteractWith(elem);
        }
    }
}
