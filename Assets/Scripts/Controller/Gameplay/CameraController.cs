﻿using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField] float minOrtographicSize = 2.5f;
    [SerializeField] float maxOrtographicSize = 14f;
    [SerializeField] float minTouchMoveDistance = 3f;

    float horizontalMovement;
    float verticalMovement;

    Vector2 startPos;
    Vector2 maxLevelOffset;
    Vector2 minLevelOffset;

    Vector2 minMovementBounds;
    Vector2 maxMovementBounds;

    Vector2?[] oldTouchPositions = {
        null,
        null
    };
    Vector2 oldTouchVector;
    float oldTouchDistance;

    CameraViewportAdjust cameraViewportAdjust;
    Camera gameCamera;

    void Awake()
    {
        cameraViewportAdjust = GetComponent<CameraViewportAdjust>();
        GetComponent<GameCamera>().FocusOnMapElement(LevelMap.Player);
        gameCamera = GetComponent<Camera>();
    }
    
    void Update () {
        if(Input.touchCount == 0)
        {
            oldTouchPositions[0] = null;
            oldTouchPositions[1] = null;
        }
        else if(Input.touchCount == 1)
        {
            if(oldTouchPositions[0] == null || oldTouchPositions[1] != null)
            {
                oldTouchPositions[0] = Input.GetTouch(0).position;
                oldTouchPositions[1] = null;
            }
            else
            {
                Vector2 newTouchPosition = Input.GetTouch(0).position;
                Vector3 translation = transform.TransformDirection((Vector3)((oldTouchPositions[0] - newTouchPosition) * gameCamera.orthographicSize / gameCamera.pixelHeight * 2f));

                UpdateMovementBounds();
                float clampedX = Mathf.Clamp(transform.position.x + translation.x, -minMovementBounds.x, maxMovementBounds.x);
                float clampedY = Mathf.Clamp(transform.position.y + translation.y, -minMovementBounds.y, maxMovementBounds.y);
                transform.position = new Vector3(clampedX, clampedY, transform.position.z);

                oldTouchPositions[0] = newTouchPosition;
            }
        }
        else
        {
            if(oldTouchPositions[1] == null)
            {
                oldTouchPositions[0] = Input.GetTouch(0).position;
                oldTouchPositions[1] = Input.GetTouch(1).position;
                oldTouchVector = (Vector2)(oldTouchPositions[0] - oldTouchPositions[1]);
                oldTouchDistance = oldTouchVector.magnitude;
            }
            else {
                Vector2 screen = new Vector2(GetComponent<Camera>().pixelWidth, GetComponent<Camera>().pixelHeight);
                
                Vector2[] newTouchPositions = {
                    Input.GetTouch(0).position,
                    Input.GetTouch(1).position
                };
                Vector2 newTouchVector = newTouchPositions[0] - newTouchPositions[1];
                float newTouchDistance = newTouchVector.magnitude;

                if(newTouchDistance > minTouchMoveDistance)
                {
                    transform.position += transform.TransformDirection((Vector3)((oldTouchPositions[0] + oldTouchPositions[1] - screen) * gameCamera.orthographicSize / screen.y));
                    //transform.localRotation *= Quaternion.Euler(new Vector3(0, 0, Mathf.Asin(Mathf.Clamp((oldTouchVector.y * newTouchVector.x - oldTouchVector.x * newTouchVector.y) / oldTouchDistance / newTouchDistance, -1f, 1f)) / 0.0174532924f));
                    float ortSize = gameCamera.orthographicSize * oldTouchDistance / newTouchDistance;
                    gameCamera.orthographicSize = Mathf.Clamp(ortSize, minOrtographicSize, maxOrtographicSize);
                    transform.position -= transform.TransformDirection((newTouchPositions[0] + newTouchPositions[1] - screen) * gameCamera.orthographicSize / screen.y);

                    oldTouchPositions[0] = newTouchPositions[0];
                    oldTouchPositions[1] = newTouchPositions[1];
                    oldTouchVector = newTouchVector;
                    oldTouchDistance = newTouchDistance;

                    cameraViewportAdjust.adjustViewport();
                }
            }
            
        }

        #if UNITY_EDITOR
        if(Input.GetAxis("Horizontal") != 0)
            horizontalMovement = Input.GetAxis("Horizontal") / Mathf.Abs(Input.GetAxis("Horizontal"));
        else
            horizontalMovement = 0;

        if(Input.GetAxis("Vertical") != 0)
            verticalMovement = Input.GetAxis("Vertical") / Mathf.Abs(Input.GetAxis("Vertical"));
        else
            verticalMovement = 0;

        Vector2 camTranslation = new Vector2(horizontalMovement, verticalMovement).normalized * 0.5f;
        transform.position = transform.position + new Vector3(camTranslation.x, camTranslation.y, 0);
        UpdateMovementBounds();
        float x = Mathf.Clamp(transform.position.x, -minMovementBounds.x, maxMovementBounds.x);
        float y = Mathf.Clamp(transform.position.y, -minMovementBounds.y, maxMovementBounds.y);
        transform.position = new Vector3(x, y, transform.position.z);
        #endif
    }

    void UpdateMovementBounds()
    {
        float cameraHeight = 2 * minOrtographicSize;
        float cameraWidth = cameraHeight * gameCamera.aspect;

        maxMovementBounds = MaxLevelOffset - new Vector2(cameraWidth, cameraHeight) / 4;
        minMovementBounds = MinLevelOffset - new Vector2(cameraWidth, cameraHeight) / 4;
    }

    public Vector2 MinLevelOffset
    {
        get
        {
            return minLevelOffset;
        }

        set
        {
            minLevelOffset = value;
        }
    }
    public Vector2 MaxLevelOffset
    {
        get
        {
            return maxLevelOffset;
        }

        set
        {
            maxLevelOffset = value;
        }
    }

    public Vector2 StartPos
    {
        get
        {
            return startPos;
        }

        set
        {
            startPos = value;
        }
    }

}
