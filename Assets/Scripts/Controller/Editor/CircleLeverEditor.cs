﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects] [CustomEditor(typeof(CircleLever))]
public class CircleLeverEditor : Editor {

    SerializedProperty statesCount;
    SerializedProperty barsState0;
    SerializedProperty barsState1;
    SerializedProperty barsState2;
    SerializedProperty barsState3;

    private void OnEnable()
    {
        barsState0 = serializedObject.FindProperty("barsState0");
        barsState1 = serializedObject.FindProperty("barsState1");
        barsState2 = serializedObject.FindProperty("barsState2");
        barsState3 = serializedObject.FindProperty("barsState3");
        statesCount = serializedObject.FindProperty("statesCount");
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.PropertyField(statesCount);
        EditorGUILayout.PropertyField(barsState0, true);
        EditorGUILayout.PropertyField(barsState1, true);

        if(statesCount.intValue > 2)
            EditorGUILayout.PropertyField(barsState2, true);
        if(statesCount.intValue > 3)
            EditorGUILayout.PropertyField(barsState3, true);

        serializedObject.ApplyModifiedProperties();
    }
}
