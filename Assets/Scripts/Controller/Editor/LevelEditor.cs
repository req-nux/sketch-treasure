﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelEditorMap))]
public class LevelEditor : Editor {

    GameObject[] prefabs;
    GameObject selectedPrefab;
    List<GameObject> spawnedGO = new List<GameObject>();

    GameObject selectedGameObject;
    Transform map;

    public override void OnInspectorGUI()
    {

        map = GameObject.Find("Map").transform;

        //Load all prefabs as objects from the 'Prefabs' folder
        Object[] obj = Resources.LoadAll("Prefabs/Gameplay/MapElements", typeof(GameObject));

        //initialize the game object array
        prefabs = new GameObject[obj.Length];

        //store the game objects in the array
        for(int i = 0; i < obj.Length; i++)
        {
            prefabs[i] = (GameObject)obj[i];
        }

        GUILayout.BeginHorizontal();

        if(prefabs != null)
        {
            int elementsInThisRow = 0;
            for(int i = 0; i < prefabs.Length; i++)
            {
                elementsInThisRow++;

                //get the texture from the prefabs
                Texture prefabTexture = prefabs[i].transform.GetComponentInChildren<SpriteRenderer>().sprite.texture;

                //create one button for earch prefabs 
                //if a button is clicked, select that prefab and focus on the scene view
                if(GUILayout.Button(prefabTexture, GUILayout.MaxWidth(50), GUILayout.MaxHeight(50)))
                {
                    selectedPrefab = prefabs[i];
                    EditorWindow.FocusWindowIfItsOpen<SceneView>();

                }

                //move to next row after creating a certain number of buttons so it doesn't overflow horizontally
                if(elementsInThisRow > Screen.width / 70)
                {
                    elementsInThisRow = 0;
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                }
            }
        }
        GUILayout.EndHorizontal();

        //EditorGUILayout.LabelField("Chunk size", EditorStyles.boldLabel);
        DrawDefaultInspector();
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("levelToLoad"));
        if(GUILayout.Button("Reload level", GUILayout.MaxWidth(100), GUILayout.MaxHeight(30)))
        {
            clearLevel();
            LevelConfig config = ((LevelConfig)(serializedObject.FindProperty("levelToLoad").objectReferenceValue));
            GameObject.FindObjectOfType<LevelBuilder>().RebuildLevel(config);
        }
        GUILayout.Space(100);
        EditorGUILayout.LabelField("REMEMBER TO ALLIGN CAMERA!", EditorStyles.boldLabel);
        if(GUILayout.Button("Overwrite level", GUILayout.MaxWidth(100), GUILayout.MaxHeight(30)))
        {
            LevelConfig config = ((LevelConfig)(serializedObject.FindProperty("levelToLoad").objectReferenceValue));
            if(config != null)
            {
                LevelConfigSaver.SaveLevel(config, true);
                Debug.Log("Level overwritten");
            }
            else
                Debug.LogWarning("Set LevelConfig to overwrite in LevelEditor inspector!");
        }
    }

    void OnSceneGUI()
    {
        Handles.BeginGUI();
        GUILayout.Box("Map Edit Mode");
        GUILayout.Box("Press 'E' to create object.");
        GUILayout.Box("Press 'X' to delete last object.");
        GUILayout.Box("Press 'Shift + D' to delete walls, fields,\nelements and spiders.");

        if(selectedPrefab == null)
            GUILayout.Box("No prefab selected!");

        Handles.EndGUI();

        Vector3 mousePosition = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin;
        Vector3 spawnPosition = snapToInteger(mousePosition);

        //if 'E' pressed, spawn the selected prefab
        if(Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.E)
            Spawn(spawnPosition);

        //if 'X' is pressed, undo (remove the last spawned prefab)
        if(Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.X)
        {
            if(spawnedGO.Count > 0)
            {
                DestroyImmediate(spawnedGO[spawnedGO.Count - 1]);
                spawnedGO.RemoveAt(spawnedGO.Count - 1);
            }
        }

        if(Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.F && Event.current.shift )
            Debug.Log("shift");

        //if 'shift + d' is pressed, delete all walls, fields, elements, spiders
        if(Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.D && Event.current.shift)
            clearLevel();

        if(selectedGameObject != null)
            Handles.Label(selectedGameObject.transform.position, "X");

        //used to indicate the exact point where the prefab will be instantiated
        Handles.CircleCap(0, spawnPosition, Quaternion.Euler(0, 0, 0), .05f);

        SceneView.RepaintAll();
    }

    void clearLevel()
    {
        Debug.Log("Level cleared");
        var t = GameObject.Find("Walls").transform;
        while(t.childCount > 0)
            DestroyImmediate(t.GetChild(0).gameObject);
        t = GameObject.Find("Fields").transform;
        while(t.childCount > 0)
            DestroyImmediate(t.GetChild(0).gameObject);
        t = GameObject.Find("Elements").transform;
        while(t.childCount > 0)
            DestroyImmediate(t.GetChild(0).gameObject);
        t = GameObject.Find("Spiders").transform;
        while(t.childCount > 0)
            DestroyImmediate(t.GetChild(0).gameObject);
        spawnedGO.Clear();
    }

    
    void Spawn(Vector2 pos)
    {
        GameObject go = (GameObject)Instantiate(selectedPrefab, new Vector3(pos.x, pos.y, 0), Quaternion.identity);
        selectedGameObject = go;
        go.name = selectedPrefab.name;
        setParentByName(go);
        spawnedGO.Add(go);
    }

    void setParentByName(GameObject go)
    {
        if(go.name == "Wall")
            go.transform.SetParent(map.Find("Walls"));
        else if(go.name == "Field")
            go.transform.SetParent(map.Find("Fields"));
        else if(go.name == "Spider")
        {
            go.transform.SetParent(map.Find("Spiders"));
            go.SetActive(true);
        }
        else
            go.transform.SetParent(map.Find("Elements"));

    }

    Vector3 snapToInteger(Vector3 val)
    {
        return new Vector3(Mathf.RoundToInt(val.x), Mathf.RoundToInt(val.y), 0);
    }

}


