﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Custom editor option used to create and save SegmentUnlockingDependencies as asset
/// </summary>
public static class CreateSegmentDependenciesMenuItem
{

    [MenuItem("Level/Create Segment Unlocking Dependencies object")]
    public static void CreateDependenciesToAsset()
    {
        if(GameObject.FindObjectOfType<MapSegment>() == null)
        {
            Debug.LogError("MapSegment must be present in scene to create SegmentUnlockingDependencies!");
            return;
        }

        MapSegment[] segments = GameObject.FindObjectsOfType<MapSegment>();
        SegmentUnlockingDependencies sud = ScriptableObject.CreateInstance<SegmentUnlockingDependencies>();
        sud.Dependecies = new SegmentDependencies[segments.Length];
        for(int i = 0; i < segments.Length; i++)
            sud.Dependecies[i] = new SegmentDependencies(segments[i].Level.ID,segments[i].UnlockedSegmentsIDs);

        AssetDatabase.CreateAsset(sud, "Assets/Resources/SegmentUnlockingDependencies.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = sud;
        Debug.Log("Created Segment Unlocking Dependencies");
        
    }
}