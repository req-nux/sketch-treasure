﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects][CustomEditor(typeof(Spider))]
public class SpiderEditor : Editor
{
    SerializedProperty pathPoints;
    private void OnEnable()
    {
        pathPoints = serializedObject.FindProperty("pathPoints");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if(GUILayout.Button("Add path point", GUILayout.MaxWidth(200), GUILayout.MaxHeight(30)))
        {
            var spider = ((Spider)serializedObject.targetObject);
            var point = new Vector2(spider.transform.position.x, spider.transform.position.y);
            pathPoints.InsertArrayElementAtIndex(pathPoints.arraySize);
            pathPoints.GetArrayElementAtIndex(pathPoints.arraySize - 1).vector2Value = point;
            serializedObject.ApplyModifiedProperties();
        }
    }
}