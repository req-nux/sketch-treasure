﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Custom editor option used to create and save LevelConfig as asset
/// </summary>
public static class SaveLevelMenuItem {

    [MenuItem("Level/Save Level to asset")]
    public static void SaveLevelToAsset()
    {
        LevelConfig levelConfig = ScriptableObject.CreateInstance<LevelConfig>();

        LevelConfigSaver.SaveLevel(levelConfig);

        AssetDatabase.CreateAsset(levelConfig, "Assets/Resources/Levels/NewLevel.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = levelConfig;
    }
}
