﻿using CommonEngine.ScriptableObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : CommonEngine.Utils.MonoBehaviourSingleton<SoundManager> {

    [SerializeField] AudioSource musicSource;
    [SerializeField] AudioSource[] fxSources;

    [SerializeField] AudioClip backgroundMusic;
    float startMusicVolume;
    IEnumerator turnMusicVolumeCoroutine;

    void Awake() {
        if(Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

        startMusicVolume = musicSource.volume;

        musicSource.clip = backgroundMusic;
        musicSource.loop = true;
        musicSource.Play();
        musicSource.volume = MusicEnabled ? startMusicVolume : 0;

    }

    public void PlayFX(AudioObject audioObject, bool randomPitch = false, float startTime = 0)
    {
        if(PlayerPrefs.GetInt("fxEnabled", 1) == 0)
            return;

        audioObject.Play(GetUnusedAudioSource(), randomPitch, startTime);
    }
    public void StopClip(AudioClip clip)
    {
        for(int i = 0; i < fxSources.Length; i++)
        {
            if(fxSources[i].clip == clip)
                fxSources[i].Stop();
        }
    }
    public bool MusicEnabled
    {
        get { return PlayerPrefs.GetInt("musicEnabled", 1) == 1; }
        set
        {
            PlayerPrefs.SetInt("musicEnabled", value ? 1 : 0);
            musicSource.volume = value ? DefaultMusicVolume : 0;
        }
    }
    public bool FxEnabled
    {
        get { return PlayerPrefs.GetInt("fxEnabled", 1) == 1; }
        set
        {
            PlayerPrefs.SetInt("fxEnabled", value ? 1 : 0);
            if(!value)
            {
                foreach(var source in fxSources)
                {
                    source.Stop();
                }
            }
        }
    }
    public float DefaultMusicVolume
    {
        get { return startMusicVolume; }
    }

    public void TurnMusicVolume(float time, float volume)
    {
        if(!MusicEnabled)
            return;

        if(turnMusicVolumeCoroutine != null)
            StopCoroutine(turnMusicVolumeCoroutine);

        if(musicSource.volume != volume)
        {
            turnMusicVolumeCoroutine = TurnMusicSmoothly(time, volume);
            StartCoroutine(turnMusicVolumeCoroutine);
        }
    }
    public void SetMusicVolume(float volume)
    {
        if(MusicEnabled)
        {
            musicSource.volume = volume;
        }
    }

    IEnumerator TurnMusicSmoothly(float time, float newVolume)
    {
        float volumeStep = (newVolume - musicSource.volume) / time;
        while(Mathf.Abs(musicSource.volume - newVolume) > volumeStep)
        {
            musicSource.volume += volumeStep * Time.deltaTime;
            yield return null;
        }
        musicSource.volume = newVolume;
        turnMusicVolumeCoroutine = null;
    }

    AudioSource GetUnusedAudioSource()
    {
        for(int i = 0; i < fxSources.Length; i++)
        {
            if(!fxSources[i].isPlaying)
                return fxSources[i];
        }
        return gameObject.AddComponent<AudioSource>();
    }

}
