﻿using UnityEngine;

public class SystemButtonsController : MonoBehaviour {

    void Update () {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            #if UNITY_ANDROID
                AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
                activity.Call<bool>("moveTaskToBack", true);
            #endif
        }
    }
}
