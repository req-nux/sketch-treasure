﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace CommonEngine
{
    public class Translations : ScriptableObject
    {
        [SerializeField] TextAsset translationsFile;
        [SerializeField] char splitToken = ',';

        Dictionary<string, Dictionary<string, string>> translations;

        string defaultLanguage = SystemLanguage.English.ToString();
        string currentLanguage;

        void OnEnable()
        {
            if(translationsFile != null)
            {
                SetLanguage(Application.systemLanguage.ToString());
                InitTranslations(translationsFile.text);
            }
            else
            {
                Debug.LogWarning("Translations file missing!");
            }
        }

        public string GetTextByKey(string key)
        {
            return translations.ContainsKey(currentLanguage)
                ? translations[currentLanguage][key]
                : translations[defaultLanguage][key];
        }
        public void SetLanguage(string language)
        {
            currentLanguage = language;
        }

        void InitTranslations(string translationsText)
        {
            if(string.IsNullOrEmpty(translationsText))
            {
                translationsText = System.Text.Encoding.Default.GetString(translationsFile.bytes);
                if(string.IsNullOrEmpty(translationsText))
                {
                    Debug.LogError("Translations file is empty!");
                    return;
                }
            }
            translations = new Dictionary<string, Dictionary<string, string>>();
            StringReader reader = new System.IO.StringReader(translationsText);

            string line = reader.ReadLine();
            string[] languages = line.Substring(1).Split(splitToken);
            foreach(string language in languages)
            {
                translations.Add(language, new Dictionary<string, string>());
            }

            while((line = reader.ReadLine()) != null)
            {
                string[] splitLine = line.Split(splitToken);
                for(int i = 0; i < languages.Length; i++)
                {
                    translations[languages[i]].Add(splitLine[0], splitLine[i + 1]);
                }
            }
        }
    }
}