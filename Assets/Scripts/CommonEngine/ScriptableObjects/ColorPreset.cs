﻿using UnityEngine;

namespace CommonEngine.ScriptableObjects
{
    public class ColorPreset : ScriptableObject
    {

        public Color color;

        public static implicit operator Color(ColorPreset preset)
        {
            return preset.color;
        }

        public float r
        {
            get { return color.r; }
        }
        public float g
        {
            get { return color.g; }
        }
        public float b
        {
            get { return color.b; }
        }
        public float a
        {
            get { return color.a; }
        }
    }
}