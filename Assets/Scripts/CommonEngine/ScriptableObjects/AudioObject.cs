﻿using UnityEngine;

namespace CommonEngine.ScriptableObjects
{
    public class AudioObject : ScriptableObject
    {
        [SerializeField] AudioClip clip;
        [SerializeField] float defaultVolume = 1;

        [SerializeField] float defaultPitch = 1;
        [SerializeField] float minPitch = 0;
        [SerializeField] float maxPitch = 0;

        /// <summary>
        /// Plays clip using passed AudioSource;
        /// </summary>
        /// <param name="source">AudioSource that will play clip</param>
        /// <param name="randomPitch">Should use random pitch?</param>
        /// <param name="startTime">Start point of clip in % (i.e. 0.3f)</param>
        public void Play(AudioSource source, bool randomPitch = false, float startTime = 0)
        {
            source.volume = defaultVolume;
            if(randomPitch)
            {
                if(minPitch == maxPitch)
                {
                    Debug.LogWarning("AudioObject: You are trying to play audio with random pitch, " +
                        "when minPitch and maxPith are equal!");
                }
                else
                {
                    source.pitch = Random.Range(minPitch, maxPitch);
                }
            }
            else
            {
                source.pitch = defaultPitch;
            }

            source.clip = clip;
            source.time = clip.length * startTime;
            source.Play();
        }

        public AudioClip Clip { get { return clip; } }
        public float ClipLength { get { return clip.length; } }
    }
}
