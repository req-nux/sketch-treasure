﻿using UnityEngine;

public static class Vector3Extensions {

    public static Vector2 XY(this Vector3 v3)
    {
        return new Vector2(v3.x, v3.y);
    }

    public static Vector2 Abs(this Vector2 v)
    {
        return new Vector2(Mathf.Abs(v.x), Mathf.Abs(v.y));
    }

    public static Vector3 SetX(this Vector3 v, float newX)
    {
        return new Vector3(newX, v.y, v.z);
    }
}
