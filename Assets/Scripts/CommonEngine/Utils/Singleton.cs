﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonEngine.Utils
{
    public class Singleton<T> where T : class
    {
        static T instance = null;

        public static T Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = (T)System.Activator.CreateInstance(typeof(T));
                }
                return instance;
            }
        }

    }
}