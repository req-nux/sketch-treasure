﻿using UnityEditor;
using UnityEngine;
using CommonEngine.ScriptableObjects;

public static class ScriptableObjectsFactory
{

    [MenuItem("CommonEngine/ScriptableObjects/Create ColorPreset")]
    public static void CreateNewColorPreset()
    {
        var colorPreset = ScriptableObject.CreateInstance<ColorPreset>();
        SaveAndFocusOn(colorPreset, "Assets/Resources/ColorPresets/NewColorPreset.asset");
    }

    [MenuItem("CommonEngine/ScriptableObjects/Create AudioObject")]
    public static void CreateNewAudioObject()
    {
        var audioObject = ScriptableObject.CreateInstance<AudioObject>();
        SaveAndFocusOn(audioObject, "Assets/Resources/Audio/AudioObjects/NewAudioObject.asset");
    }
    [MenuItem("CommonEngine/ScriptableObjects/Create Translations")]
    public static void CreateNewTranslations()
    {
        var audioObject = ScriptableObject.CreateInstance<CommonEngine.Translations>();
        SaveAndFocusOn(audioObject, "Assets/Resources/NewTranslations.asset");
    }

    private static void SaveAndFocusOn(Object createdObject, string path)
    {
        AssetDatabase.CreateAsset(createdObject, path);
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = createdObject;
    }
}