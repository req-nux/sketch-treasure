﻿using UnityEngine;
using UnityEditor;

namespace CommonEngine.Utils.Editor
{
    public static class ClearPlayerPrefs
    {
        [MenuItem("CommonEngine/Clear PlayerPrefs")]
        public static void DoClearPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("PlayerPrefs cleared!");
        }
    }
}