﻿using UnityEngine;
using UnityEngine.UI;

namespace CommonEngine
{
    [RequireComponent (typeof(Text))]
    public class CustomText : MonoBehaviour
    {
        enum CustomTextMode
        {
            ToUpper,
            ToLower,
            FirstCapital,
            EveryFirstCapital,
            Unchanged
        }

        [SerializeField] Translations translations;
        [SerializeField] string key;
        [SerializeField] CustomTextMode mode = CustomTextMode.Unchanged;


        void Start()
        {
            SetText(translations.GetTextByKey(key));
        }

        public void SetTextByKey(string key)
        {
            this.key = key;
            SetText(translations.GetTextByKey(key));
        }

        void SetText(string text)
        {
            switch(mode)
            {
                case CustomTextMode.Unchanged: break;
                case CustomTextMode.FirstCapital:
                    {
                        char[] chars = text.ToCharArray();
                        chars[0] = char.ToUpper(chars[0]);
                        text = new string(chars);
                        break;
                    }
                case CustomTextMode.EveryFirstCapital:
                    {
                        var words = text.Split(' ');
                        char[] chars;
                        text = string.Empty;
                        for(int i = 0; i < words.Length; i++)
                        {
                            chars = words[i].ToCharArray();
                            chars[0] = char.ToUpper(chars[0]);
                            text += new string(chars);
                            if(i + 1 < words.Length)
                                text += " ";
                        }
                        break;
                    }
                case CustomTextMode.ToLower: text = text.ToLower(); break;
                case CustomTextMode.ToUpper: text = text.ToUpper(); break;
            }
            GetComponent<Text>().text = text;
        }

        void Reset()
        {
            var translationsObject = Resources.Load<Translations>("Translations");
            if(translationsObject != null)
                translations = translationsObject;
        }
    }
}