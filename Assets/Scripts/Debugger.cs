﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debugger : MonoBehaviour {

#if UNITY_EDITOR
    static Debugger instance;
    void Start () {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
            
    }
    
    void Update () {
        if(Input.GetKeyDown(KeyCode.T))
        {
            var pc = GameObject.FindObjectOfType<PlayerController>();
            pc.teleportation = !pc.teleportation;
            Debug.Log("Debugger : Teleportation = " + pc.teleportation);
        }
        if(Input.GetKeyDown(KeyCode.K))
        {
            var p = GameObject.FindObjectOfType<Player>();
            p.Death();
            Debug.Log("Debugger : Player killed!");
        }
        if(Input.GetKeyDown(KeyCode.N))
        {
            GameplayManager.Instance.OnLevelComplete();
        }
        if(Input.GetKeyDown(KeyCode.M))
        {
            if(Time.timeScale == 1)
                Time.timeScale = 0.1f;
            else
                Time.timeScale = 1f;
        }
    }
#endif
}
